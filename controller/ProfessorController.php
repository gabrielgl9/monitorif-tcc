<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (!session_id()) {
    session_start();
}

if (isset($_POST["gerenciarDisciplinas"])) {
    ProfessorController::gerenciarDisciplinas();
}

class ProfessorController {

    public static function gerenciarDisciplinas() {

        $professorModel = new Professor();
        $professorModel->deletarDisciplinasPorProfessor($_SESSION["matricula"]);
        foreach ($_POST["disciplinas"] as $value) {
            $professorModel->adicionarDisciplina($value, $_SESSION["matricula"]);
        }
        $_SESSION['sweet'] = "Dados cadastrados com sucesso!";
        header("location: ../view/telaGerenciarMaterias.php");
    }

    public static function verificaSeLecionaDisciplina($idDisciplina, $matricula) {

        $professorModel = new Professor();
        return $professorModel->verificaSePossuiDisciplina($idDisciplina, $matricula);
    }

    public static function buscarDisciplinasDoProfessor($matricula) {

        $professorModel = new Professor();
        return $professorModel->buscarDisciplinasDoProfessor($matricula);
    }

    public static function verificaSeExisteProfessor($matricula, $nomeProfessor) {

        $professorModel = new Professor();
        $status = $professorModel->verificarSeExisteProfessor($matricula);
        if ($status) {
            return $status;
        } else {
            return $professorModel->cadastrarProfessor($matricula, $nomeProfessor);
        }
    }

}

?>