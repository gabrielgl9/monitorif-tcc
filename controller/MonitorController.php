<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (!session_id()) {
    session_start();
}

if (isset($_POST['cadastrarMonitor'])) {
    MonitorController::cadastrarMonitor();
}

if (isset($_POST['editarMonitor'])) {
    MonitorController::atualizarMonitor($_POST['mat']);
}
if (isset($_POST['idExcluir'])) {
    MonitorController::deletarMonitor($_POST['idExcluir']);
}

class MonitorController {

    public static function cadastrarMonitor() {

        extract($_POST);
        $nome = filter_var($nome, FILTER_SANITIZE_STRING);
        $matricula = filter_var($matricula, FILTER_SANITIZE_STRING);
        if (isset($disciplinas)) {
            $monitorModel = new Monitor();
            $monitorModel->setNome($nome);
            $monitorModel->setMatricula($matricula);
            $idMonitor = $monitorModel->CadastrarMonitor();

            $disciplinaModel = new Disciplina();
            foreach ($disciplinas as $value) {
                $monitorModel->inserirMonitorNaDisciplina($value, $idMonitor);
                $disciplinaModel->soma($value);
            }

            $_SESSION['sweet'] = "Monitor cadastrado com sucesso!";
            if (isset($_SESSION["professor"])) {
                header("location:../view/telaCadastrarMonitor.php");
            } else {
                header("location:../view/telaAdmin.php");
            }
        } else {
            header("location:../view/telaCadastrarMonitor.php");
        }
    }

    public static function mostrarDisciplinasDeCadaMonitor($idMonitor) {

        $monitoriModel = new Monitor();
        return $monitoriModel->mostrarTodasDisciplinasDeCadaMonitor($idMonitor);
    }

    public static function mostrarTodosMonitores() {

        $monitorModel = new Monitor();
        return $monitorModel->mostrarTodosMonitores();
    }

    public static function deletarMonitor($matricula) {

        $monitorModel = new Monitor();
        $disciplinaModel = new Disciplina();
        $disciplinaModel->descontaDisciplinas($matricula);
        $monitorModel->deletarDisciplinaDoMonitor($matricula);
        $monitorModel->excluirMonitor($matricula);
        if (isset($_SESSION["professor"])) {
            header("location:../view/telaObservarMonitores.php");
        } else {
            header("location:../view/telaAdmin.php");
        }
    }

    public static function atualizarMonitor($mat) {

        extract($_POST);
        $nome = filter_var($nome, FILTER_SANITIZE_STRING);
        $matricula = filter_var($matricula, FILTER_SANITIZE_STRING);
        if (isset($disciplinas)) {
            $monitorModel = new Monitor();
            $monitorModel->setNome($nome);
            $monitorModel->setMatricula($matricula);
            $monitorModel->editarMonitor($mat);
            $disciplinaModel = new Disciplina();
            $disciplinaModel->descontaDisciplinas($matricula);
            $idMonitor = $monitorModel->deletarDisciplinaDoMonitor($matricula);

            foreach ($disciplinas as $value) {
                $monitorModel->inserirMonitorNaDisciplina($value, $idMonitor);
                $disciplinaModel->soma($value);
            }
        }
        $_SESSION['sweet'] = "Os dados do monitor foram atualizados com sucesso!";
        if (isset($_SESSION["professor"])) {
            header("location:../view/telaObservarMonitores.php");
        } else {
            header("location:../view/telaAdmin.php");
        }
    }

    public static function load() {
        $monitorModel = new Monitor();
        $arr = $monitorModel->mostrarTodosMonitores();
        foreach ($arr as $valor) {
            if ($valor[1] == $_SESSION['matricula']) {
                $dados[] = $valor[0];
                $dados[] = $valor[2];
            }
        }
        return $dados;
    }

    public static function possuiDisciplina($idDisciplina, $idMonitor) {
        $monitorModel = new Monitor();
        return $monitorModel->procurarDisciplina($idDisciplina, $idMonitor);
    }

    public static function mostrarMonitoresPorDisciplina($idDisciplina) {

        $monitorModel = new Monitor();
        return $monitorModel->mostrarMonitoresPorDisciplina($idDisciplina);
    }

}
