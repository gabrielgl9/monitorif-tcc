<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (!session_id()) {
    session_start();
}

if (isset($_POST['comentario'])) {
    ComentarioController::fazerComentario();
}

class ComentarioController {

    public static function fazerComentario() {

        $texto = filter_var($_POST["texto"], FILTER_SANITIZE_STRING);
        $comentarioModel = new Comentario();
        $comentarioModel->setMatricula($_POST["matricula"]);
        $comentarioModel->setIdMaterial($_POST["idMaterial"]);
        $comentarioModel->setTexto($texto);
        $comentarioModel->setNome($_POST["nome"]);
        $comentarioModel->insert();
        /*if (isset($_SESSION["professor"])) {
            header("location: ../view/telaProfessor.php");
        }
        if ($_SESSION["monitor"] == 1) {
            header("location: ../view/telaMonitor.php");
        } else if ($_SESSION["monitor"] == 0) {
            header("location: ../view/telaAluno.php");
        }*/
    }

    public static function mostraQuantidadeComentarios($idMaterial) {

        $comentarioModel = new Comentario();
        return $comentarioModel->mostraQuantidadeComentarios($idMaterial);
    }

    public static function mostraTodosComentarios($idMaterial) {

        $comentarioModel = new Comentario();
        return $comentarioModel->mostraTodosComentarios($idMaterial);
    }

}
