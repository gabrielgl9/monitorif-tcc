<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}


if (isset($_GET['idExcluir'])) {
    ArquivoTicketController::excluirArquivoPorId($_GET['idExcluir']);
}

class ArquivoTicketController {

    public static function salvarArquivo($codigoTicket) {

        $arquivo = isset($_FILES['arquivo']) ? $_FILES['arquivo'] : FALSE;
        for ($k = 0; $k < count($arquivo['name']); $k++) {
            $caminho = '../upload/ticket/' . self::concatenaNomeComId($arquivo['name'][$k], $codigoTicket);
            if ($arquivo['size'][$k] < 30000000 && move_uploaded_file($arquivo['tmp_name'][$k], $caminho)) {
                $arquivoModel = new ArquivoTicket();
                $arquivoModel->setNomeVerdadeiro($arquivo['name'][$k]);
                $arquivoModel->setNome(self::concatenaNomeComId($arquivo['name'][$k], $codigoTicket));
                $arquivoModel->setTipo($arquivo['type'][$k]);
                $arquivoModel->setCodigoTicket($codigoTicket);
                $arquivoModel->inserirArquivo();
            } else {
                echo "droga! passou o limite máximo";
            }
        }
    }

    public static function loadArquivoTicket($codigoTicket) {

        $arquivoModel = new ArquivoTicket();
        $arquivoModel->setCodigoTicket($codigoTicket);
        return $arquivoModel->carregarArquivoPorTicket();
    }

    public static function excluirArquivoPorTicket($codigoTicket) {

        $arquivoModel = new ArquivoTicket();
        $arquivoModel->setCodigoTicket($codigoTicket);
        $arquivoModel->excluirArquivoPorTicket();
    }

    public static function excluirArquivoPorId($id) {

        $arquivoModel = new ArquivoTicket();
        $arr = $arquivoModel->carregarArquivoPorId($id);
        $arquivoModel->excluirArquivoPorId($id);
        header("location:../view/telaEditandoTicket.php");
    }

    private function concatenaNomeComId($texto, $codigo) {

        $posicao = strpos($texto, '.');
        $parte1 = substr($texto, 0, $posicao);
        $parte1 = $parte1 . $codigo;
        $parte2 = substr($texto, $posicao);
        return $parte1 . $parte2;
    }

}
