<?php

class Autoloader {

    public function register($className) {
        
        //todos os diretórios do sistema.
        $directorys = array(
            '../controller/',
            '../model/',
            '../view/',
        );
        
        //foreach percorrendo todos os diretórios.
        foreach($directorys as $directory){
            //verifica se a classe existe em algum diretório.
            if(file_exists($directory. $className . '.php')){
                //importa o arquivo.
                require_once($directory . $className . '.php');
                return true;
            }           
        }
    }
}