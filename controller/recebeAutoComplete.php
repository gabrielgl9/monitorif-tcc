<?php
require_once '../controller/Autoloader.php';
session_start();
spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

$var = unserialize($_SESSION['user']);
?>

<script>
    var estado;
    $(".curtir").click(function () {
        if ($(this).attr("title") === "curtir" || estado === "curtir") {
            var ident = parseInt($(this).attr("alt"));
            $(this).children().css("color", "blue");
            estado = $(this).attr("title", "descurtir");
            $(".total").each(function (idx, el) {
                if (idx === ident) {
                    $(el).text(parseInt($(el).text()) + 1 + " pessoas curtiram este material");

                }
            })

            $.post("../controller/MaterialController.php",
                    {
                        curtida: "curtida",
                        idMaterial: $(this).attr("name"),
                    }, function (data, status) {
                //alert(data, status)

            });
        } else if ($(this).attr("title") === "descurtir" || estado === "descurtir") {
            var ident = parseInt($(this).attr("alt"));
            $(this).children().css("color", "black");
            estado = $(this).attr("title", "curtir");
            $(".total").each(function (idx, el) {
                if (idx === ident) {
                    $(el).text(parseInt($(el).text()) - 1 + " pessoas curtiram este material");

                }
            })
            $.post("../controller/MaterialController.php",
                    {
                        descurtida: "descurtida",
                        idMaterial: $(this).attr("name"),
                    }, function (data, status) {

            });

        }
    });


    $(".comentario").click(function () {
        var ident = parseInt($(this).attr("name"));
        var nome = $(this).attr("alt");
        $(".materialize-textarea").each(function (idx, el) {
            if (idx === ident) {
                var texto = $(el).val();
                $.post("../controller/ComentarioController.php",
                        {
                            comentario: "comentario",
                            idMaterial: $(el).attr("name"),
                            texto: texto,
                            nome: nome,
                            matricula: <?php echo $_SESSION["matricula"]; ?>
                        }, function (data, status) {
                    $(el).val(" ");
                    swal('Obrigado por deixar a sua opinião aqui', ' ', 'success');
                    $(".novoComentario").each(function (i, e) {
                        if (ident === i) {
                            $(e).after("<div class='aux-coment indigo lighten-5'><p><span class='blue-text'> " + nome + ": </span> " + texto + "</p></div><br>");
                        }
                    });
                });
            }
        });
    });

</script>

<style>
    .aux-coment{
        border-radius:3px;
        border: 1px solid;
        border-color: white;
    }
</style>
<?php
$count = 0;
if (!empty(MaterialController::autoComplete($_GET["valor"]))):
    foreach (MaterialController::autoComplete($_GET["valor"]) as $value):
        if (isset($_SESSION["professor"])) {
            ?>                 
            <div class="professor" name="<?php echo $value[3]; ?>">
            <?php } else { ?>
                <div class="pesquisaDisciplina" name=<?php echo $value[6]; ?>>
                <?php } ?>
                <div class="row">
                    <div class="offset-s3 offset-m4 col s8 m6">
                        <ul class="card">
                            <li class="blue-grey">
                                <div class="row" style="padding: 10px">
                                    <div class="col s3">
                                        <img src="<?php echo $value[1]; ?>"  class="circle responsive-img valign profile-image cyan">
                                    </div>
                                    <div>
                                        <a class="col s5 btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav"><?php echo $value[3]; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                                        <?php //<span class="col s4 activator white-text btn-flat"><i class="material-icons right">more_vert</i></span>
                                        ?><p class="col s7 user-roal white-text"><?php echo $value[6]; ?></p>
                                    </div>   
                                </div>
                            </li>
                            <li style="margin-left: 10px;"><p style="font-size: 20px;"><?php echo $value[5]; ?></p></li>
                            <li>
                                <p class="center" style="font-size: 20px;"><?php echo $value[4]; ?></p>
                            </li>
                            <li class="row">
                                <p class="center-align" style="font-size: 20px;">R - <?php echo $value[2]; ?></p>
                            </li>
                            <div class="row center">
                                <?php
                                $array = ArquivoMaterialController::loadArquivoMaterial($value[0]);
                                if (!empty($array)):
                                    foreach ($array as $valor) :
                                        if ($valor[2] == "image/gif" || $valor[2] == "image/jpeg" || $valor[2] == "image/jpg" || $valor[2] == "image/png") {
                                            ?> <a class="modal-trigger" href="#modal2<?php echo $valor[0]; ?>"><img src="../upload/material/<?php echo $valor[1]; ?>" class="responsive-img" style="width: 190px; height: 270px;"></a>
                                            <?php
                                        } elseif ($valor[2] == "video/mp4") {
                                            ?> <video class="responsive-video" controls><source src="../upload/material/<?php echo $valor[1]; ?>" type="video/mp4"></video>
                                            <?php } else { ?>
                                            <a href="../upload/material/<?php echo $valor[1]; ?>"> <?php echo $valor[4]; ?> </a>
                                        <?php } ?>
                                        <div id="modal2<?php echo $valor[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <img src="../upload/material/<?php echo $valor[1]; ?>" class="responsive-img">
                                            </div>
                                            <div class="modal-footer">
                                                <a href="" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                            </div>
                                        </div>

                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>

                            <li style="margin-left: 10px; margin-right: 10px;"> 
                                <?php
                                $links = MaterialController::mostrarLinks($value[0]);
                                if (isset($links)):
                                    echo "<p style='font-size:18px;'>Links externos:</p>";
                                    foreach ($links as $val) :
                                        ?>
                                        <a href="<?php echo $val[1] ?> "><?php echo $val[1]; ?></a><br>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </li>
                            <div class="divider"></div>
                            <li class="row" style="margin-top: 2%;">
                                <?php
                                if (MaterialController::possuiCurtida($value[0], $_SESSION["matricula"])):
                                    ?>
                                    <div name="<?php echo $value[0]; ?>" class="curtir" title="descurtir" alt="<?php echo $count; ?>">
                                        <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                            <i class="material-icons" btn-flat blue-text>thumb_up</i>
                                        </a><span class=" col s2" style="margin-left: -4%; color: blue">Curtir</span>
                                    </div>
                                <?php else: ?>
                                    <div name="<?php echo $value[0]; ?>" class="curtir" title="curtir" alt="<?php echo $count; ?>">
                                        <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                            <i class="material-icons" btn-flat grey-text>thumb_up</i>
                                        </a><span class="col s2" style="margin-left: -4%;">Curtir</span>
                                    </div>
                                <?php endif; ?>
                                <div class="activator">
                                    <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                        <i class="material-icons">sms</i>
                                    </a><span class="col s3 m2 l1" style="margin-left: -4%;">Comentar</span>
                                </div>
                            </li>
                            <li style="margin-top: -2%; margin-left: 2%; font-size: 15px;" class="grey-text">
                                <div  class="total" name="<?php echo $value[0]; ?>">
                                    <span> <?php echo MaterialController::mostraQuantidadeCurtidasPorMaterial($value[0]); ?>
                                    </span> pessoas curtiram este material
                                </div>
                            </li>
                            <div class="card-reveal blue-grey">
                                <span class="card-title grey-text white-text">Faça seu comentário<i class="material-icons right">close</i></span>
                                <br>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea placeholder="Comente aqui" class="materialize-textarea indigo lighten-5" name="<?php echo $value[0]; ?>" style="border-radius:3px; border: 1px solid; border-color: white;"></textarea>
                                    </div>
                                    <button class="btn right waves-effect waves-light comentario" name="<?php echo $count; ?>" alt="<?php echo $var->getNome(); ?>" type="submit">Enviar
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>

                                <?php
                                $comentarios = ComentarioController::mostraTodosComentarios($value[0]);
                                if (isset($comentarios)):
                                    ?><span class="grey-text white-text" style="font-size: 20px;">
                                        comentários:
                                    </span><br><br>
                                    <div class="novoComentario indigo lighten-5" name="<?php echo $count; ?>">
                                    </div>
                                    <?php
                                    foreach ($comentarios as $v):
                                        ?>
                                        <div class="indigo lighten-5" style="border-radius:3px; border: 1px solid; border-color: white;">
                                            <p><span class="blue-text"><?php echo $v[4] . " "; ?>:</span> <?php echo $v[1]; ?></p>
                                        </div><br>
                                        <?php
                                    endforeach;
                                else:
                                    ?>
                                    <span class = "grey-text white-text" style = "font-size: 20px;">
                                        Seja o primeiro a comentar
                                    </span>
                                    <br><br>
                                    <div class="novoComentario indigo lighten-5" name="<?php echo $count; ?>">
                                    </div>
                                <?php
                                endif;
                                ?>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
            $count++;
        endforeach;
    endif;
    ?>

