<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (isset($_POST["cadastrar"])) {
    DisciplinaController::cadastrarDisciplina($_POST["nomeDisciplina"]);
}

if (isset($_POST["cadastrarPendente"])) {
    DisciplinaController::cadastrarDisciplinaPendente($_POST["nomeDisciplina"], $_POST["nomeProfessor"]);
}

if (isset($_POST["idExcluir"])) {
    DisciplinaController::excluirDisciplina($_POST["idExcluir"]);
}

class DisciplinaController {

    public static function cadastrarDisciplina($nome) {

        $disciplinaModel = new Disciplina();
        if (isset($_POST["id"])) {
            $disciplinaModel->setId($_POST["id"]);
        }
        $disciplinaModel->setNome($nome);
        $disciplinaModel->insert();
        if (isset($_POST["id"])) {
            header("location: ../view/telaGerenciarDisciplinas.php");
        }
    }

    public static function cadastrarDisciplinaPendente($nomeDisciplina, $nomeProfessor) {

        $disciplinaModel = new Disciplina();
        $disciplinaModel->cadastrarDisciplinaPendente($nomeDisciplina, $nomeProfessor);
    }

    public static function excluirDisciplina($id) {

        $disciplinaModel = new Disciplina();
        $disciplinaModel->delete($id);
        header("location: ../view/telaGerenciarDisciplinas.php");
    }

    public static function procurarDisciplina($id) {

        $disciplinaModel = new Disciplina();
        return $disciplinaModel->procurarDisciplina($id);
    }

    public static function mostrarTodasDisciplinasPendentes() {

        $disciplinaModel = new Disciplina();
        return $disciplinaModel->mostrarTodasDisciplinasPendentes();
    }

    public static function mostrarTodasDisciplinas() {

        $disciplinaModel = new Disciplina();
        return $disciplinaModel->getAll();
    }

    public static function mostrarTodasDisciplinasDoProfessor($matricula) {

        $disciplinaModel = new Disciplina();
        return $disciplinaModel->mostrarDisciplinasDoProfessor($matricula);
    }

}
