<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

if (isset($_POST['cadastrarTicket'])) {
    TicketController::cadastarTicket();
}

if (isset($_POST['editarTicket'])) {
    TicketController::editarTicket($_POST['idTicket']);
}

if (isset($_POST['idExcluir'])) {
    TicketController::removerTicket($_POST['idExcluir']);
}

class TicketController {

    public static function cadastarTicket() {

        extract($_POST);
        $descricao = filter_var($descricao, FILTER_SANITIZE_STRING);
        $conteudo = filter_var($conteudo, FILTER_SANITIZE_STRING);
        $disciplina = filter_var($disciplina, FILTER_SANITIZE_STRING);

        $ticketModel = new Ticket();
        $ticketModel->setDescricao($descricao);
        $ticketModel->setConteudo($conteudo);
        $ticketModel->setDisciplina($disciplina);
        $codigoTicket = $ticketModel->inserirNovoTicket();
        if ($codigoTicket != -5) {
            if (isset($_FILES['arquivo']['name'])) {
                ArquivoTicketController::salvarArquivo($codigoTicket);
            }
            $_SESSION['sweet'] = "Ticket enviado com sucesso! Aguarde a resposta do monitor.";
        } else {
            $_SESSION['sweet'] = "Ticket não foi enviado, pois não temos nenhum monitor dessa disciplina :( ";
        }
        header("location:../view/telaGerenciarTicket.php");
    }

    public static function ativarPendencia($codTicket) {

        $ticketModel = new Ticket();
        $ticketModel->ativarPendencia($codTicket);
    }

    public static function desativarPendencia($codTicket) {

        $ticketModel = new Ticket();
        $ticketModel->desativarPendencia($codTicket);
    }

    public static function editarTicket($id) {

        extract($_POST);
        $descricao = filter_var($descricao, FILTER_SANITIZE_STRING);
        $conteudo = filter_var($conteudo, FILTER_SANITIZE_STRING);
        $disciplina = filter_var($disciplina, FILTER_SANITIZE_STRING);

        $ticketModel = new Ticket();
        $ticketModel->setDescricao($descricao);
        $ticketModel->setConteudo($conteudo);
        $ticketModel->setDisciplina($disciplina);
        $ticketModel->modificarTicket($id);
        if (isset($_FILES['arquivo']['name'])) {
            ArquivoTicketController::salvarArquivo($id);
        }
        $_SESSION['sweet'] = "Ticket modificado com sucesso!";

        header("location:../view/telaGerenciarTicket.php");
    }

    public function removerTicket($id) {

        $ticketModel = new Ticket();
        $ticketModel->deletarTicketPendente($id);
        $_SESSION['sweet'] = "Ticket excluido com sucesso!";
        header("location:../view/telaGerenciarTicket.php");
    }

    public static function mostrarSeusTickets() {

        $ticketModel = new Ticket();
        return $ticketModel->mostrarSeusTickets();
    }

    public static function mostrarTodosTicketsAtivos() {

        $ticketModel = new Ticket();
        return $ticketModel->mostrarTodosTicketsAtivos();
    }

    public static function mostrarTodosTickets() {

        $ticketModel = new Ticket();
        return $ticketModel->mostrarTodosTicketsPendentes();
    }

}
