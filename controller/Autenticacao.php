<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

$usuario = filter_input(INPUT_POST, 'matricula');
$senha = filter_input(INPUT_POST, 'senha');
if (MoodleUtil::geraToken($usuario, $senha)) {
    $obj = MoodleUtil::requisita('core_webservice_get_site_info');
    $url = str_replace('pluginfile.php', 'webservice/pluginfile.php', $obj->userpictureurl);
    $urlToken = 'https://moodle.canoas.ifrs.edu.br/login/token.php?username=' . $usuario . '&password=' . $senha . '&service=moodle_mobile_app';
    $str = json_decode(file_get_contents($urlToken));
    $user = new Aluno();
    $user->setNome($obj->fullname);
    $user->setFoto($url);
    $user->setIdUsuario($obj->userid);
    $user->setToken($str->token);
    $user->setMatricula($usuario);
    $_SESSION['user'] = serialize($user);
    $arr = MonitorController::mostrarTodosMonitores();
    if (identificaUsuario($usuario) == "02") {
        foreach ($arr as $value) {
            if ($value[1] == $usuario) {
                $_SESSION['matricula'] = $value[1];
                header("location:../view/telaMonitor.php");
                exit();
            }
        }
        header('location: ../view/telaAluno.php');
    } else {
        $_SESSION['matricula'] = $usuario;
        header('location: ../view/telaProfessor.php');
    }
} else {
    session_destroy();
    if ($usuario == "admin" and $senha == "admin") {
        header("location:../view/telaAdmin.php");
    } else {
        session_start();
        $_SESSION["sweet"] = "Falha na autenticação";
        $_SESSION["matriculaError"] = $usuario;
        header('location: ../view/telaLogin.php');
    }
}

function identificaUsuario($text) {
    $parteInicial = substr($text, 0, 2);
    return $parteInicial;
}
