<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (!session_id()) {
    session_start();
}


if (isset($_POST['cadastrarMaterial'])) {
    MaterialController::cadastarMaterial();
}

if (isset($_POST["curtida"])) {
    MaterialController::curtirMaterial($_POST['idMaterial'], $_SESSION["matricula"]);
}


if (isset($_POST["descurtida"])) {
    MaterialController::descurtirMaterial($_POST['idMaterial'], $_SESSION["matricula"]);
}

if (isset($_POST['editarMaterial'])) {
    MaterialController::editarMaterial($_POST['idMaterial']);
}

if (isset($_POST['idExcluir'])) {
    MaterialController::excluirMaterial($_POST['idExcluir']);
}

class MaterialController {

    public static function cadastarMaterial() {

        extract($_POST);
        $explicacao = filter_var($explicacao, FILTER_SANITIZE_STRING);
        $codTicket = filter_var($codTicket, FILTER_SANITIZE_STRING);

        $materialModel = new Material();
        $materialModel->setImagem($_SESSION["foto"]);
        $materialModel->setExplicacao($explicacao);
        $materialModel->setCodigoTicket($codTicket);
        $materialModel->cadastrarMaterial();
        TicketController::desativarPendencia($codTicket);
        foreach (MaterialController::mostrarTodosMateriais() as $value) {
            if ($value[7] == $codTicket):
                $idMaterial = $value[0];
            endif;
        }

        if (isset($_FILES['arquivo']['name'])) {
            ArquivoTicketController::salvarArquivo($idMaterial);
        }

        $linkModel = new Link();

        if (isset($links[0]) && !empty($links[0])) {
            foreach ($links as $valor) {
                if (!empty($valor)) {
                    $linkModel->insert("$valor", $idMaterial);
                }
            }
        }

        $_SESSION['sweet'] = "Material elaborado com sucesso!";
        header("location:../view/telaGerenciarMaterial.php");
    }

    public static function editarMaterial($codMaterial) {

        extract($_POST);
        $explicacao = filter_var($explicacao, FILTER_SANITIZE_STRING);
        $codTicket = filter_var($codTicket, FILTER_SANITIZE_STRING);
        $idTicketAntigo = filter_var($idTicketAntigo, FILTER_SANITIZE_STRING);
        TicketController::ativarPendencia($idTicketAntigo);

        $materialModel = new Material();
        $materialModel->setExplicacao($explicacao);
        $materialModel->setCodigoTicket($codTicket);
        $materialModel->modificarMaterial($codMaterial);
        TicketController::desativarPendencia($codTicket);
        foreach (MaterialController::mostrarTodosMateriais() as $value) {
            if ($value[7] == $codTicket):
                $idMaterial = $value[0];
            endif;
        }

        if (isset($_FILES['arquivo']['name'])) {
            ArquivoTicketController::salvarArquivo($idMaterial);
        }
        $linkModel = new Link();
        $linkModel->excluir($idMaterial);

        if (isset($links[0]) && !empty($links[0])) {
            foreach ($links as $valor) {
                if (!empty($valor)) {
                    $linkModel->insert("$valor", $idMaterial);
                }
            }
        }

        $_SESSION['sweet'] = "Material modificado com sucesso!";
        header("location:../view/telaGerenciarMaterial.php");
    }

    public static function excluirMaterial($codMaterial) {


        $array = MaterialController::mostrarTodosMateriais();
        foreach ($array as $value) {
            if ($value[0] == $codMaterial) {
                $idTicket = $value[7];
            }
        }
        TicketController::ativarPendencia($idTicket);
        $materialModel = new Material();
        $materialModel->excluirMaterial($codMaterial);
        ArquivoMaterialController::excluirArquivoPorMaterial($codMaterial);

        $linkModel = new Link();
        $linkModel->excluir($codMaterial);

        $_SESSION['sweet'] = "Material excluido com sucesso!";
        header("location:../view/telaGerenciarMaterial.php");
    }

    public static function mostrarSeusMateriais() {

        $materialModel = new Material();
        $array = MonitorController::mostrarTodosMonitores();
        foreach ($array as $value) {
            if ($_SESSION['matricula'] == $value[1]) {
                $idMonitor = $value[2];
            }
        }
        return $materialModel->mostrarSeusMateriais($idMonitor);
    }

    public static function mostrarTodosMateriais() {

        $materialModel = new Material();
        return $materialModel->mostrarTodosMateriaisDisponibilizados();
    }

    public static function autoComplete($conteudo) {
        $materialModel = new Material();
        return $materialModel->autoComplete($conteudo);
    }

    public static function curtirMaterial($idMaterial, $matricula) {

        $materialModel = new Material();
        $materialModel->curtirMaterial($matricula, $idMaterial);
    }

    public static function mostraQuantidadeCurtidasPorMaterial($idMaterial) {
        $materialModel = new Material();
        return $materialModel->mostraQuantidadeCurtidasPorMaterial($idMaterial);
    }

    public static function descurtirMaterial($idMaterial, $matricula) {

        $materialModel = new Material();
        $materialModel->removerCurtida($matricula, $idMaterial);
    }

    public static function mostrarLinks($idMaterial) {

        $linkModel = new Link();
        return $linkModel->mostrarLinks($idMaterial);
    }

    public function possuiCurtida($idMaterial, $matricula) {
        $materialModel = new Material();
        return $materialModel->verificaSeCurtiu($idMaterial, $matricula);
    }

    public static function mostraMateriaisPorDisciplina() {
        $materialModel = new Material();
        return $materialModel->mostraMateriaisPorDisciplina();
    }

}
