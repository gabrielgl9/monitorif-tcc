<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

if (isset($_GET['idExcluir'])) {
    ArquivoMaterialController::excluirArquivoPorId($_GET['idExcluir']);
}

class ArquivoMaterialController {

    public static function salvarArquivo($codigoMaterial) {

        $arquivo = isset($_FILES['arquivo']) ? $_FILES['arquivo'] : FALSE;
        for ($k = 0; $k < count($arquivo['name']); $k++) {
            $caminho = '../upload/material/' . self::concatenaNomeComId($arquivo['name'][$k], $codigoMaterial);
            if ($arquivo['size'][$k] < 3000000000000000000000000 && move_uploaded_file($arquivo['tmp_name'][$k], $caminho)) {
                $arquivoModel = new ArquivoMaterial();
                $arquivoModel->setNomeVerdadeiro($arquivo['name'][$k]);
                $arquivoModel->setNome(self::concatenaNomeComId($arquivo['name'][$k], $codigoMaterial));
                $arquivoModel->setTipo($arquivo['type'][$k]);
                $arquivoModel->setCodigoMaterial($codigoMaterial);
                $arquivoModel->inserirArquivo();
            } else {
                echo "droga! passou o limite máximo";
            }
        }
    }

    public static function loadArquivoMaterial($codigoMaterial) {

        $arquivoModel = new ArquivoMaterial();
        $arquivoModel->setCodigoMaterial($codigoMaterial);
        return $arquivoModel->carregarArquivoPorMaterial();
    }

    public static function excluirArquivoPorMaterial($codigoMaterial) {

        $arquivoModel = new ArquivoMaterial();
        $arquivoModel->setCodigoMaterial($codigoMaterial);
        $arquivoModel->excluirArquivoPorMaterial();
    }

    public static function excluirArquivoPorId($id) {

        $arquivoModel = new ArquivoMaterial();
        $arr = $arquivoModel->carregarArquivoPorId($id);
        $arquivoModel->excluirArquivoPorId($id);
        header("location:../view/telaEditandoMaterial.php");
    }

    private function concatenaNomeComId($texto, $codigo) {

        $posicao = strpos($texto, '.');
        $parte1 = substr($texto, 0, $posicao);
        $parte1 = $parte1 . $codigo;
        $parte2 = substr($texto, $posicao);
        return $parte1 . $parte2;
    }

}
