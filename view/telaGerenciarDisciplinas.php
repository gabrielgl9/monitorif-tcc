<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
?>

<?php
if (!session_id()) {
    session_start();
}
?>


<html>
    <head>
        <title>Gerenciar Disciplinas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();
            $("#cadastrar").click(function () {
                swal("Digite o nome da disciplina:", {
                    content: "input",
                })
                        .then((value) => {
                            if (value !== '' && value.replace(/ /g, "") !== "") {
                                $.ajax({
                                    url: '../controller/DisciplinaController.php',
                                    type: 'post',
                                    data: {
                                        cadastrar: "cadastrar",
                                        nomeDisciplina: value
                                    }
                                }).done(function () {
                                    swal(`Você cadastrou a disciplina: ${value}`).then(function () {
                                        location.reload();
                                    });
                                });
                            } else {
                                swal(`Cadastro de disciplina inválido: ${value}`);
                            }
                        });
            });
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <ul class="right">
                    <li><a id="cadastrar" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Cadastrar Disciplina</a></li>
                    <li><a href="telaPendenciasDisciplina.php" class="waves-effect waves-block waves-light notification-button"><i class="material-icons">notifications_none</i></a></li>
                    <li style="margin-left: -4%;">
                        <small class="notification-badge">
                            <span class="new badge">
                                <?php
                                if (empty(DisciplinaController::mostrarTodasDisciplinasPendentes())) {
                                    echo 0;
                                } else {
                                    echo count(DisciplinaController::mostrarTodasDisciplinasPendentes());
                                }
                                ?>
                            </span>
                        </small>
                    </li>
                </ul>
                <ul>
                    <li><a href="telaAdmin.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br>

        <h3 class="center"> GERENCIAR DISCIPLINAS </h3><br>

        <?php
        $array = DisciplinaController::mostrarTodasDisciplinas();
        if (!empty($array)):
            ?>
            <div class="row">
                <div class="col s8 offset-s2 white">
                    <table class="centered responsive-table bordered white">
                        <thead>
                            <tr>
                                <th>Nome da disciplina</th>
                                <th>Número de monitores</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($array as $value):
                                ?>
                                <tr>
                                    <td><?php echo $value[1]; ?></td>
                                    <td><?php echo $value[2]; ?></td>
                                    <td>
                                        <a href="#modal2<?php echo $value[0]; ?>" class="red-text modal-trigger"><i class='material-icons'>delete</i></a>

                                        <div id="modal2<?php echo $value[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Você tem certeza que deseja excluir esse ticket?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <form method="POST" action="../controller/DisciplinaController.php" >
                                                    <input type="hidden" name="idExcluir" value="<?php echo $value[0]; ?>"/>
                                                    <a href="#!" class="modal-close waves-effect waves-green btn red">Cancelar</a>
                                                    <button class="modal-close waves-effect waves-green btn" type="submit">Confirmar</button>    
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>