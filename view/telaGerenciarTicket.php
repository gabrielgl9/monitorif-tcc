<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}
?>


<html>
    <head>
        <title>Gerenciar Ticket</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();

        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Tickets em pendência</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="hide-on-med-and-down">
                    <?php if ($_SESSION['monitor'] == 0) { ?>
                        <li><a href="telaAluno.php"><i class="material-icons left">keyboard_return</i></a></li>
                    <?php } else { ?>
                        <li><a href="telaMonitor.php"><i class="material-icons left">keyboard_return</i></a></li>
                    <?php } ?>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="telaTicket.php" class="waves-effect waves-light btn"><i class="material-icons left">sms</i>Estou com dúvida</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="telaTicket.php"><i class="material-icons left">sms</i>Estou com dúvida</a></li>
                    <?php if ($_SESSION['monitor'] == 0) { ?>
                        <li><a href="telaAluno.php"><i class="material-icons left">keyboard_return</i>Voltar para tela principal</a></li>
                    <?php } else { ?>
                        <li><a href="telaMonitor.php"><i class="material-icons left">keyboard_return</i>Voltar para tela principal</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>

        <br><br>

        <?php
        $arr = TicketController::mostrarSeusTickets();
        if (!empty($arr)):
            ?>
            <div class="row">
                <div class="col s10 offset-s1 white">
                    <table class="centered responsive-table bordered white" >
                        <thead>
                            <tr>
                                <th>Descrição</th>
                                <th>Conteúdo</th>
                                <th>Disciplina</th>
                                <th>Arquivo(s)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($arr as $value):
                                ?>
                                <tr>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[1]; ?> </p></td>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[2]; ?> </p></td>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[3]; ?></p> </td>
                                    <td>
                                        <?php
                                        $array = ArquivoTicketController::loadArquivoTicket($value[0]);
                                        if (!empty($array)):
                                            foreach ($array as $valor) :
                                                if ($valor[2] == "image/gif" || $valor[2] == "image/jpeg" || $valor[2] == "image/jpg" || $valor[2] == "image/png") {
                                                    ?>
                                                    <a class="blue-text modal-trigger" href="#modal3<?php echo $valor[0]; ?>"><?php echo $valor[4]; ?></a> <br>
                                                <?php } else { ?>
                                                    <a href="../upload/ticket/<?php echo $valor[1]; ?>"> <?php echo $valor[4]; ?></a> <br>
                                                <?php } ?>

                                                <div id="modal3<?php echo $valor[0]; ?>" class="modal">
                                                    <div class="modal-content">
                                                        <img src="../upload/ticket/<?php echo $valor[1]; ?>" class="responsive-img">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                                    </div>
                                                </div>

                                                <?php
                                            endforeach;
                                        else:
                                            echo "Não foi utilizado nenhum arquivo";
                                        endif;
                                        ?>
                                    </td>
                                    <td>
                                        <form method="POST" action="telaEditandoTicket.php">
                                            <a href="#modal2<?php echo $value[0]; ?>" class="red-text modal-trigger"><i class='material-icons'>delete</i></a>
                                            <input type="hidden" name="idEditar" value="<?php echo $value[0]; ?>">
                                            <a class="green-text btn-flat" onclick='this.parentNode.submit(); return false;' style="margin-top:-10%;"><i class="material-icons">edit</i></a>
                                            <a class="blue-text modal-trigger" href="#modal1<?php echo $value[0]; ?>"><i class='material-icons'>visibility</i></a>
                                        </form>

                                        <div id="modal1<?php echo $value[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Informações do Ticket</h5>
                                                <p><b>Conteudo:</b> <?php echo $value[2]; ?></p>
                                                <p><b>Descrição do problema:</b> <?php echo $value[1]; ?></p>
                                                <p><b>Disciplina:</b> <?php echo $value[3]; ?></p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                            </div>
                                        </div>
                                        <div id="modal2<?php echo $value[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Você tem certeza que deseja excluir esse ticket?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <form method="POST" action="../controller/TicketController.php" >
                                                    <input type="hidden" name="idExcluir" value="<?php echo $value[0]; ?>"/>
                                                    <a href="#!" class="modal-close waves-effect waves-green btn red">Cancelar</a>
                                                    <button class="modal-close waves-effect waves-green btn" type="submit">Confirmar</button>    
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                    <?php
                                endforeach;
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        endif;
        ?>

        <?php
        if (!empty($_SESSION['sweet'])):
            if ($_SESSION['sweet'] == "Ticket modificado com sucesso!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você editou um ticket!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else if ($_SESSION['sweet'] == "Ticket excluido com sucesso!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você excluiu um ticket!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else if ($_SESSION['sweet'] == "Ticket enviado com sucesso! Aguarde a resposta do monitor.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Parabéns! Essa atitude demonstra interesse em aprender.', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Mas podemos solicitar que tenha!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>
    </body>
</html>