<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

if (isset($_POST["idEditar"])) {
    $_SESSION["id"] = null;
    $_SESSION["id"] = $_POST["idEditar"];
}
?>


<html>
    <head>
        <title>Editando Material</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $(".material-icons").click(function () {
                $(this).siblings(".removeLink").attr("value", "").remove();
                $(this).remove();
            });
            $(".addLink").click(function () {
                $(".controlador").append("<input type='text' placeholder='Insira um link' name='links[]' class='validate'>");
            });
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Editando um material</a>
                <ul>
                    <li><a href="telaGerenciarMaterial.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br>
        <?php
        $arr = MaterialController::mostrarSeusMateriais();
        foreach ($arr as $value) :
            if ($value[0] == $_SESSION["id"]):
                $array = array($value[0], $value[1], $value[2], $value[3], $value[4], $value[5]);
            endif;
        endforeach;
        ?>
        <div class="row">
            <div class="card-panel col s8 offset-s2">
                <div class="card-content horizontal">
                    <div class="row">
                        <form method="post" action="../controller/MaterialController.php" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="editarMaterial"/>
                                <input type="hidden" value="<?php echo $array[0]; ?>" name="idMaterial"/>
                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                <?php
                                foreach (TicketController::mostrarTodosTicketsAtivos() as $valor) {
                                    if ($array[5] == $valor[0]) {
                                        echo "<input type='hidden' name='idTicketAntigo' value='$valor[0]'/>";
                                    }
                                }
                                ?>

                                <div class="input-field col s12 required">
                                    <i class="material-icons prefix">library_books</i>
                                    <select name="codTicket">
                                        <?php
                                        foreach (TicketController::mostrarTodosTicketsAtivos() as $valor) {
                                            if ($array[5] == $valor[0]) {
                                                echo "<option value='$valor[0]' selected> $valor[1]</option>";
                                            }
                                        }

                                        foreach (TicketController::mostrarTodosTickets() as $valor) {
                                            echo "<option value='$valor[0]'> $valor[1]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $matriz = ArquivoMaterialController::loadArquivoMaterial($_SESSION["id"]);
                            echo '<div class="row container">';
                            if (!empty($matriz)):
                                foreach ($matriz as $value) :
                                    if ($value[2] == "image/gif" || $value[2] == "image/jpeg" || $value[2] == "image/jpg" || $value[2] == "image/png") {
                                        echo '<div class="col s4"><img src="../upload/material/' . $value[1] . '" class="responsive-img" style="width:100px; height:100px;">';
                                        echo '<center><a href="../controller/arquivoMaterialController.php?idExcluir=' . $value[0] . '"><i class="material-icons red-text">delete</i></a></center></div>';
                                    } elseif ($value[2] == "video/mp4") {
                                        echo '<div class="col s4"><video class="responsive-video" controls><source src="../upload/material/' . $value[1] . '" type="video/mp4"></video>';
                                        echo '<center><a href="../controller/arquivoMaterialController.php?idExcluir=' . $value[0] . '"><i class="material-icons red-text">delete</i></a></center></div>';
                                    } else {
                                        echo '<div class="col s3"><a href="../upload/material/' . $value[1] . '"> Arquivo externo </a>';
                                        echo '<center><a href="../controller/arquivoMaterialController.php?idExcluir=' . $value[0] . '"><i class="material-icons red-text">delete</i></a></center></div>';
                                    }
                                endforeach;
                            endif;
                            echo '</div>';
                            ?>

                            <div class="row">
                                <div class="input-field col s12 required">
                                    <i class="material-icons prefix">school</i>
                                    <textarea id="icon_prefix2" name="explicacao" class="materialize-textarea" style="border: 1px solid; border-color: grey; border-style: dashed;"><?php echo $array[2]; ?></textarea>
                                    <label for="icon_prefix2">Explicação:</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="file-field">
                                    <div class="btn">
                                        <span>Envie um arquivo</span>
                                        <input id="arquivo" name="arquivo[]" type="file" multiple >
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Arquivo(s)">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                if (isset(MaterialController::mostrarLinks($_SESSION["id"])[0])):
                                    foreach (MaterialController::mostrarLinks($_SESSION["id"]) as $val):
                                        ?>
                                        <div class="input-field col s11">
                                            <input type='text' placeholder='Insira um link' name='links[]' value="<?php echo $val[1]; ?>" class='removeLink validate'>
                                            <i class="material-icons prefix red-text btn-flat">close</i>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>

                            <div class="row">
                                <a class="addLink btn-flat blue-grey white-text">Link externo</a>
                            </div>

                            <div class="controlador input-field col s12 required">
                            </div>


                            <div class="row">
                                <div class="input-field col s6">
                                    <a href="telaGerenciarMaterial.php" class="waves-effect waves-light btn red" style="width: 100%;">Cancelar</a>
                                </div>
                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Editar</button>                            
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
