<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
?>

<?php
if (!session_id()) {
    session_start();
}
$var = unserialize($_SESSION['user']);
?>


<html>
    <head>
        <title>Gerenciar Disciplinas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();
            $("#cadastrar").click(function () {
                swal("Digite o nome da disciplina:", {
                    content: "input",
                })
                        .then((value) => {
                            if (value !== '' && value.replace(/ /g, "") !== "") {
                                $.ajax({
                                    url: '../controller/DisciplinaController.php',
                                    type: 'post',
                                    data: {
                                        cadastrarPendente: "cadastrarPendente",
                                        nomeDisciplina: value,
                                        nomeProfessor: $(this).attr("name"),
                                    }
                                }).done(function () {
                                    swal(`Você solicitou o cadastro da disciplina: ${value}`).then(function () {
                                        location.reload();
                                    });
                                });
                            } else {
                                swal(`Cadastro de disciplina inválido: ${value}`);
                            }
                        });
            });
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <ul class="right">
                    <li><a id="cadastrar" name="<?php echo $var->getNome(); ?>" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Solicitar Nova Disciplina</a></li>
                </ul>
                <ul>
                    <li><a href="telaProfessor.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>

            </div>
        </nav>
        <br>
        <h3 class="center"> Disciplinas Disponíveis </h3><br>

        <br><br>
        <?php
        if (!empty(DisciplinaController::mostrarTodasDisciplinas())):
            ?>
            <div class="row">
                <div class="card-panel col s10 m8 offset-s1 offset-m2">
                    <div class="card-content horizontal">
                        <div class="row">
                            <form method="post" action="../controller/ProfessorController.php" enctype="multipart/form-data">
                                <div class="row required" style="margin:10px;">
                                    <input type="hidden" name="gerenciarDisciplinas" value="gerenciarDisciplinas"/>
                                    <?php
                                    $x = 0;
                                    foreach (DisciplinaController::mostrarTodasDisciplinas() as $value): $x++;
                                        if (ProfessorController::verificaSeLecionaDisciplina($value[0], $_SESSION["matricula"])):
                                            ?>
                                            <div class="col s3 m3" style="padding: 15px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>" checked="checked"/>
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                            <?php
                                        else:
                                            ?>
                                            <div class="col s3 m3" style="padding: 15px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>"   />
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </div>
                                <div class="input-field">
                                    <div class="input-field col s6">
                                        <a href="telaProfessor.php" class="waves-effect waves-light btn red" style="width: 100%;">Voltar</a>
                                    </div>
                                    <div class="input-field col s6">
                                        <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Enviar</button>                            
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        endif;
        ?>

        <?php
        if (!empty($_SESSION['sweet'])):
            echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você definiu suas disciplinas!', 'success');</script>";
            $_SESSION['sweet'] = null;
        endif;
        ?>

    </body>
</html>