<?php
require_once '../controller/Autoloader.php';


if (!session_id()) {
    session_start();
}

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (isset($_POST["idEditar"])) {
    $_SESSION["id"] = null;
    $_SESSION["id"] = $_POST["idEditar"];
}
?>

<html>
    <head>
        <title>Editando um Ticket</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Editar Ticket de atendimento</a>
                <ul>
                    <li><a href="telaGerenciarTicket.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br><br>
        <?php
        $arr = TicketController::mostrarSeusTickets();
        foreach ($arr as $value) :
            if ($value[0] == $_SESSION["id"]):
                $array = array($value[0], $value[1], $value[2], $value[3]);
            endif;
        endforeach;
        ?>
        <div class="row">
            <div class="card-panel col s8 offset-s2">
                <div class="card-content horizontal">
                    <div class="row">
                        <form method="post" action="../controller/TicketController.php" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" value="editarTicket" name="editarTicket"/>
                                <input type="hidden" value="<?php echo $array[0]; ?>" name="idTicket"/>
                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
                                <div class="input-field col s6">
                                    <i class="material-icons prefix">library_books</i>
                                    <select name="disciplina" required> 
                                        <option value="<?php echo $array[3]; ?>"><?php echo $array[3]; ?></option>
                                        <?php
                                        foreach (DisciplinaController::mostrarTodasDisciplinas() as $value) {
                                            if ($value[2] > 0 and $value[1] != $array[3]) {
                                                echo "<option value='$value[1]'>$value[1]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="input-field col s6">
                                    <input id="conteudo" type="text" name="conteudo" class="validate required" value="<?php echo $array[2]; ?>">
                                    <label for="conteudo">Conteúdo:</label>
                                </div>
                            </div>
                            <div class="row container">
                                <?php
                                $matriz = ArquivoTicketController::loadArquivoTicket($_SESSION["id"]);
                                if (!empty($matriz)):
                                    foreach ($matriz as $value) :
                                        if ($value[2] == "image/gif" || $value[2] == "image/jpeg" || $value[2] == "image/jpg" || $value[2] == "image/png") {
                                            ?><div class="col s4"><img src="../upload/ticket/<?php echo $value[1]; ?>" class="responsive-img" style="width:100px; height:100px;">
                                                <center><a href="../controller/arquivoTicketController.php?idExcluir=<?php echo $value[0]; ?>"><i class="material-icons red-text">delete</i></a></center></div>
                                            <?php
                                        } elseif ($value[2] == "video/mp4") {
                                            ?>
                                            <div class="col s4"><video class="responsive-video" controls><source src="../upload/ticket/<?php echo $value[1]; ?>" type="video/mp4"></video>
                                                <center><a href="../controller/arquivoTicketController.php?idExcluir=<?php echo $value[0]; ?>"><i class="material-icons red-text">delete</i></a></center></div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="col s3"><a href="../upload/ticket/<?php echo $value[1]; ?>"> <?php echo $value[1]; ?> </a>';
                                                <center><a href="../controller/arquivoTicketController.php?idExcluir=<?php echo $value[0]; ?>"><i class="material-icons red-text">delete</i></a></center></div>
                                            <?php
                                        }
                                    endforeach;
                                endif;
                                ?>
                            </div>

                            <div class="row">
                                <div class="input-field col s12" >
                                    <textarea id="problema" name="descricao" class="materialize-textarea" required style="border: 1px solid; border-color: grey; border-style: dashed;"><?php echo $array[1]; ?></textarea>
                                    <label for="problema">Descreva seu problema aqui</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="file-field input-field col s12">
                                    <div class="btn">
                                        <span>Envie um arquivo</span>
                                        <input id="arquivo" name="arquivo[]" type="file" multiple>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload um ou mais arquivos"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s6">
                                    <a href="telaGerenciarTicket.php" class="waves-effect waves-light btn red" style="width: 100%;">Cancelar</a>
                                </div>
                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Editar</button>                            
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
