<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
?>

<?php
if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <title>Cadastrando Monitor</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Cadastrar Monitor</a>
                <ul>
                    <?php
                    if (isset($_SESSION["professor"])) {
                        echo "<li><a href='telaObservarMonitores.php'><i class='material-icons left'>keyboard_return</i></a></li>";
                    } else {
                        echo "<li><a href='telaAdmin.php'><i class='material-icons left'>keyboard_return</i></a></li>";
                    }
                    ?>

                </ul>
            </div>
        </nav>
        <br><br><br>

        <div class="row">
            <div class="card-panel col s8 offset-s2">
                <div class="card-content horizontal">
                    <div class="row">
                        <form method="post" action="../controller/MonitorController.php" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" value="cadastrarMonitor" name="cadastrarMonitor"/>
                                <div class="input-field col s6" >
                                    <input id="nome" name="nome" type="text" class="validate" required>
                                    <label for="nome">Nome completo</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="matricula" name="matricula" type="text" class="validate" required>
                                    <label for="matricula">Matricula</label>
                                </div>
                            </div>
                            <?php
                            if (isset($_SESSION["professor"])) {
                                $array = ProfessorController::buscarDisciplinasDoProfessor($_SESSION["matricula"]);

                                if (!empty($array)) {
                                    ?>
                                    <div class="row required">
                                        <p>Disciplina(s):</p>
                                        <?php
                                        $x = 0;
                                        foreach ($array as $value): $x++;
                                            ?>
                                            <div class="col s3 m3" style="padding: 10px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>" />
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                            <?php
                                        endforeach;
                                        ?>
                                    </div>

                                    <?php
                                }
                            } else {
                                ?>
                                <div class="row required">
                                    <p>Disciplina(s):</p>
                                    <?php
                                    $x = 0;
                                    if (isset(DisciplinaController::mostrarTodasDisciplinas()[0]) && !empty(DisciplinaController::mostrarTodasDisciplinas()[0])):
                                        foreach (DisciplinaController::mostrarTodasDisciplinas() as $value): $x++;
                                            ?>
                                            <div class="col s3 m3" style="padding: 10px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>" />
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <?php if (isset($_SESSION["professor"])) { ?>
                                    <div class="input-field col s6">
                                        <a href="telaObservarMonitores.php" class="waves-effect waves-light btn red" style="width: 100%;">Cancelar</a>
                                    </div>
                                <?php } ?>

                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Cadastrar Monitor</button>                            
                                </div>
                            </div>
                            <br><br>
                            <?php
                            if (!empty($_SESSION['sweet'])):
                                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você cadastrou um monitor!', 'success');</script>";
                                $_SESSION['sweet'] = null;
                            endif;
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
