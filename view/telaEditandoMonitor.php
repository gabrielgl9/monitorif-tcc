<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

if (isset($_POST["idEditar"])) {
    $_SESSION["id"] = null;
    $_SESSION["id"] = $_POST["idEditar"];
}
?>

<html>
    <head>
        <title>Editando Monitor</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    </head>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            Materialize.updateTextFields();
        });
    </script>
    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Editar Dados de um Monitor</a>
                <ul>
                    <?php
                    if (isset($_SESSION["professor"])) {
                        ?>
                        <li><a href="telaObservarMonitores.php"><i class="material-icons left">keyboard_return</i></a></li>
                        <?php
                    } else {
                        ?>
                        <li><a href="telaAdmin.php"><i class="material-icons left">keyboard_return</i></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </nav>
        <br><br><br><br>
        <?php
        $arr = MonitorController::mostrarTodosMonitores();
        foreach ($arr as $value) :
            if ($value[1] == $_POST['idEditar']):
                $array = array($value[0], $value[1], $value[2]);
            endif;
        endforeach;
        ?>

        <div class="row">
            <div class="card-panel col s8 offset-s2">
                <div class="card-content horizontal">
                    <div class="row">
                        <form method="post" action="../controller/MonitorController.php" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" value="editarMonitor" name="editarMonitor"/>
                                <input type="hidden" value="<?php echo $array[1]; ?>" name="mat"/>
                                <div class="input-field col s6" >
                                    <input id="nome" value="<?php echo $array[0]; ?>" name="nome" type="text" class="validate">
                                    <label for="nome" class="active">Nome completo</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="matricula" value="<?php echo $array[1]; ?>" name="matricula" type="text" class="validate">
                                    <label for="matricula" class="active">Matricula</label>
                                </div>
                            </div>


                            <div class="row">
                                <p>Disciplina(s):</p>
                                <?php
                                if (isset($_SESSION["professor"])) {
                                    $array = ProfessorController::buscarDisciplinasDoProfessor($_SESSION["matricula"]);

                                    if (!empty($array)) {

                                        $x = 0;
                                        foreach ($array as $value): $x++;
                                            if (MonitorController::possuiDisciplina($value[0], $_POST["idEditar"])):
                                                ?>
                                                <div class="col s3 m3" style="padding: 10px;">
                                                    <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>" checked="checked"/>
                                                    <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                                </div>
                                            <?php else:
                                                ?>
                                                <div class="col s3 m3" style="padding: 10px;">
                                                    <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>"   />
                                                    <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                                </div>
                                            <?php
                                            endif;
                                        endforeach;
                                    }
                                }else {
                                    $x = 0;
                                    foreach (DisciplinaController::mostrarTodasDisciplinas() as $value): $x++;
                                        if (MonitorController::possuiDisciplina($value[0], $_POST["idEditar"])):
                                            ?>
                                            <div class="col s3 m3" style="padding: 10px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>" checked="checked"/>
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                        <?php else:
                                            ?>
                                            <div class="col s3 m3" style="padding: 10px;">
                                                <input type="checkbox" name="disciplinas[]" value="<?php echo $value[0]; ?>" class="filled-in" id="test<?php echo $x; ?>"   />
                                                <label for="test<?php echo $x; ?>"><?php echo $value[1]; ?></label>
                                            </div>
                                        <?php
                                        endif;
                                    endforeach;
                                }
                                ?>
                            </div>

                            <div class="row">
                                <?php if (isset($_SESSION["professor"])) { ?>
                                    <div class="input-field col s6">
                                        <a href="telaObservarMonitores.php" class="waves-effect waves-light btn red" style="width: 100%;">Cancelar</a>
                                    </div>
                                <?php } ?>
                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Editar Monitor</button>                            
                                </div>

                            </div>
                            <br><br>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>