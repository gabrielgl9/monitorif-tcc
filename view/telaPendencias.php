<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});


if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <title>Pendências do monitor</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
    </head>    
    <script>
        $(document).ready(function () {
            $(".button-collapse").sideNav();
            $('.modal').modal();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Suas Pendências</a>
                <ul>
                    <li><a href="telaMonitor.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br><br>

        <?php
        if (!empty(TicketController::mostrarTodosTickets())):
            foreach (TicketController::mostrarTodosTickets() as $value):
                ?>
                <div class="row">
                    <div class="card-panel col s8 offset-s2">
                        <div class="card-content horizontal">
                            <div class="row">
                                <div class="col s12 l6 black-text">
                                    <p class="flow-text center-align"><b>Problema Encontrado:</b><br> <?php echo $value[1]; ?></p>
                                </div>
                                <div class="col s12 l6 black-text">
                                    <p class="flow-text  center-align"><b>Conteúdo:</b><br> <?php echo $value[2]; ?></p>
                                </div>
                            </div>
                            <div class="container center"><br>

                                <?php
                                $array = ArquivoTicketController::loadArquivoTicket($value[0]);
                                if (!empty($array)):
                                    foreach ($array as $valor) :
                                        if ($valor[2] == "image/gif" || $valor[2] == "image/jpeg" || $valor[2] == "image/jpg" || $valor[2] == "image/png") {
                                            ?> <a class="modal-trigger" href="#modal2<?php echo $valor[0]; ?>"><img src="../upload/ticket/<?php echo $valor[1]; ?>" class="responsive-img" style="height: 50%;"></a>
                                            <?php
                                        } elseif ($valor[2] == "video/mp4") {
                                            ?> <video class="responsive-video" controls><source src="../upload/ticket/<?php echo $valor[1]; ?>" type="video/mp4"></video>
                                            <?php } else { ?>
                                            <a href="../upload/ticket/<?php echo $valor[1]; ?>"> <?php echo $valor[4]; ?> </a>
                                        <?php } ?>
                                        <div id="modal2<?php echo $valor[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <img src="../upload/ticket/<?php echo $valor[1]; ?>" class="responsive-img">
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                            </div>
                                        </div>

                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                            <div class="fixed-btn right">
                                <form method="POST" action="../view/telaElaborarMaterial.php">
                                    <input type="hidden" name="idTicket" value="<?php echo $value[0]; ?>"/>
                                    <input type="hidden" name="problema" value="<?php echo $value[1]; ?>"/>
                                    <a onclick='this.parentNode.submit(); return false;' class="btn-floating btn-large red">
                                        <i class="large material-icons">mode_edit</i>
                                    </a>
                                </form>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </body>
</html>
