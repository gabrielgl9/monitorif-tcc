<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});


if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <title>Relatórios</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
    </head>    
    <script>
        $(document).ready(function () {
            $(".button-collapse").sideNav();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Relatórios</a>
                <ul>
                    <li><a href="telaObservarMonitores.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br><br>


        <?php
        //mostrarSomenteMateriaisDeDisciplinasEmQueLeciona
        if (!empty(MaterialController::mostraMateriaisPorDisciplina())):
            foreach (MaterialController::mostraMateriaisPorDisciplina() as $value):
                ?>
                <div class="professor" name="<?php echo $value[3]; ?>">
                    <div class="row">
                        <div class="offset-s2 offset-m3 col s8 m7">
                            <ul class="card">
                                <li class="blue-grey">
                                    <div class="row" style="padding: 10px">
                                        <div class="col s3">
                                            <img src="<?php echo $value[1]; ?>"  class="circle responsive-img valign profile-image cyan">
                                        </div>
                                        <div>
                                            <a class="col s5 btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav"><?php echo $value[3]; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                                            <p class="col s7 user-roal white-text"><?php echo $value[6]; ?></p>
                                        </div>   
                                    </div>
                                </li>
                                <li style="margin-left: 10px;"><p style="font-size: 20px;"><?php echo $value[5]; ?></p></li>
                                <li style="margin-left: 10px;">
                                    <p style="font-size: 18px;">O presente material foi desenvolvido no dia <b><?php echo $value[8]; ?></b> <br>
                                        e foi solicitado em <b><?php echo $value[7]; ?></b> <br>
                                        O problema encontrado pelo aluno foi: <b><?php echo $value[4]; ?></b>
                                        <br>Gerou <b><?php echo MaterialController::mostraQuantidadeCurtidasPorMaterial($value[0]); ?></b> curtidas.<!-- count(curtidas) --><br> 
                                        <?php if (ComentarioController::mostraQuantidadeComentarios($value[0]) > 0) { ?>
                                            <!-- se os comentarios forem maior que 0 -->Os comentarios foram:<br>
                                            <?php
                                            foreach (ComentarioController::mostraTodosComentarios($value[0]) as $valor) {

                                                echo "<b>• $valor[1]</b><br>";
                                            }
                                        } else {
                                            echo '<b>Não houveram comentarios para este material!</b>';
                                        }
                                        ?>
                                        <!-- se nao ***** Nenhum aluno comentou sobre o material -->
                                    </p>    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <?php
            endforeach;
        endif;
        ?>
    </body>
</html>
