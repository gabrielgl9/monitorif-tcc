<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}
?>


<html>
    <head>
        <title>Gerenciar Material</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Materiais disponibilizados</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="hide-on-med-and-down">
                    <li><a href="telaMonitor.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="telaElaborarMaterial.php" class="waves-effect waves-light btn"><i class="material-icons left">create</i>Elaborar Material</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="telaElaborarMaterial.php"><i class="material-icons left">create</i>Elaborar Material</a></li>
                    <li><a href="telaMonitor.php"><i class="material-icons left">keyboard_return</i>Voltar para tela principal</a></li>
                </ul>
            </div>
        </nav>

        <br><br>


        <?php
        $arr = MaterialController::mostrarSeusMateriais();
        if (!empty($arr)):
            ?>
            <div class="row">
                <div class="col s10 offset-s1 white">
                    <table class="centered responsive-table bordered white">
                        <thead>
                            <tr>
                                <th>Problema</th>
                                <th>Conteúdo</th>
                                <th>Explicação</th>
                                <th>Links disponibilizados</th>
                                <th>Arquivo(s):</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($arr as $value):
                                ?>
                                <tr>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[3]; ?> </p></td>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[4]; ?> </p></td>
                                    <td> <p style="width:200px;" class="truncate"><?php echo $value[2]; ?></p> </td>

                                    <td>
                                        <?php
                                        $links = MaterialController::mostrarLinks($value[0]);
                                        if (isset($links)):
                                            foreach ($links as $val) :
                                                ?>
                                                <a href="<?php echo $val[1] ?>"><?php echo $val[1]; ?></a><br>
                                                <?php
                                            endforeach;
                                        else:
                                            echo 'Não foi enviado nenhum link';
                                        endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $array = ArquivoMaterialController::loadArquivoMaterial($value[0]);
                                        if (!empty($array)):
                                            foreach ($array as $valor) :

                                                if ($valor[2] == "image/gif" || $valor[2] == "image/jpeg" || $valor[2] == "image/jpg" || $valor[2] == "image/png") {
                                                    ?>
                                                    <a class="blue-text modal-trigger" href="#modal3<?php echo $valor[0]; ?>"><?php echo $valor[4]; ?></a> <br>
                                                <?php } else { ?>
                                                    <a href="../upload/material/<?php echo $valor[1]; ?>"> <?php echo $valor[4]; ?></a> <br>
                                                <?php } ?>
                                                <div id="modal3<?php echo $valor[0]; ?>" class="modal">
                                                    <div class="modal-content">
                                                        <img src="../upload/material/<?php echo $valor[1]; ?>" class="responsive-img">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                                    </div>
                                                </div>
                                                <?php
                                            endforeach;
                                        else:
                                            echo "Não foi utilizado nenhum arquivo";
                                        endif;
                                        ?>
                                    </td>
                                    <td>
                                        <form method="POST" action="telaEditandoMaterial.php">
                                            <a href="#modal2<?php echo $value[0]; ?>" class="red-text modal-trigger"><i class='material-icons'>delete</i></a>
                                            <input type="hidden" name="idEditar" value="<?php echo $value[0]; ?>">
                                            <a class="green-text btn-flat" onclick='this.parentNode.submit(); return false;' style="margin-top:-10%;"><i class="material-icons">edit</i></a>
                                            <a class="blue-text modal-trigger" href="#modal1<?php echo $value[0]; ?>"><i class='material-icons'>visibility</i></a>
                                        </form>
                                        <div id="modal1<?php echo $value[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Informações do Material</h5><br>
                                                <p><b>Problema:</b> <?php echo $value[3]; ?></p>
                                                <p><b>Problema:</b> <?php echo $value[4]; ?></p>
                                                <p><b>Explicação:</b> <?php echo $value[2]; ?></p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                            </div>
                                        </div>
                                        <div id="modal2<?php echo $value[0]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Você tem certeza que deseja excluir esse ticket?</h5>
                                            </div>
                                            <div class="modal-footer">

                                                <form method="POST" action="../controller/MaterialController.php" >
                                                    <input type="hidden" name="idExcluir" value="<?php echo $value[0]; ?>"/>
                                                    <a href="#!" class="modal-close waves-effect waves-green btn red">Cancelar</a>
                                                    <button class="modal-close waves-effect waves-green btn" type="submit">Confirmar</button>    
                                                </form>
                                            </div>
                                        </div>

                                    </td>
                                    <?php
                                endforeach;

                                /*     <a href = "../controller/MaterialController.php?idExcluir=<?php echo $value[0]; ?>" class = "red-text" style = "margin-left: -3%;"><i class = 'material-icons'>delete</i></a>
                                  <a href = "../view/telaEditandoMaterial.php?idEditar=<?php echo $value[0]; ?>" class = "green-text"><i class = 'material-icons' style = "margin-left: 5%;">edit</i></a>
                                 */
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        endif;
        ?>

        <?php
        if (!empty($_SESSION['sweet'])):
            if ($_SESSION['sweet'] == "Material modificado com sucesso!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você editou um material!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else if ($_SESSION['sweet'] == "Material excluido com sucesso!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você excluiu um material!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você elaborou um material!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>


    </body>
</html>
