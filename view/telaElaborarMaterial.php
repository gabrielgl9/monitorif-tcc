<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}
?>


<html>
    <head>
        <title>Elaborando Material</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();
            $(".addLink").click(function () {
                $(".controlador").append("<input type='text' placeholder='Insira um link' name='links[]' class='validate'>");
            });
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Material de apoio</a>
                <ul>
                    <li><a href="telaGerenciarMaterial.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
                <div class="hide-on-med-and-down right">
                    <ul>
                        <li><a href="telaPendencias.php" class="waves-effect waves-block waves-light notification-button"><i class="material-icons">notifications_none</i></a></li>
                        <li style="margin-left: -30%;">
                            <small class="notification-badge">
                                <span class="new badge">
                                    <?php
                                    if (empty(TicketController::mostrarTodosTickets())) {
                                        echo 0;
                                    } else {
                                        echo count(TicketController::mostrarTodosTickets());
                                    }
                                    ?>
                                </span>
                            </small>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>
        <br><br>
        <div class="row">
            <div class="card-panel col s8 offset-s2">
                <div class="card-content horizontal">
                    <div class="row">
                        <form method="post" action="../controller/MaterialController.php" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="cadastrarMaterial">


                                <div class="input-field col s12 required">
                                    <i class="material-icons prefix">library_books</i>
                                    <select name="codTicket">
                                        <?php if (isset($_POST["idTicket"])): ?>
                                            <option value="<?php echo $_POST["idTicket"]; ?>"><?php echo $_POST["problema"]; ?></option>
                                        <?php else: ?>
                                            <option value="" disabled selected>Selecione a dúvida que deseja solucionar</option>
                                        <?php
                                        endif;
                                        foreach (TicketController::mostrarTodosTickets() as $valor) {
                                            echo "<option value='$valor[0]'> $valor[1]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>


                            </div>

                            <div class="row">
                                <div class="input-field col s12 required">
                                    <i class="material-icons prefix">school</i>
                                    <textarea id="icon_prefix2" name="explicacao" class="materialize-textarea" style="border: 1px solid; border-color: grey; border-style: dashed;"></textarea>
                                    <label for="icon_prefix2">Explicação:</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="file-field">
                                    <div class="btn">
                                        <span>Arquivo</span>
                                        <input id="arquivo" name="arquivo[]" type="file" multiple >
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Arquivo(s)">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <a class="addLink btn-flat blue-grey white-text">Link externo</a>
                            </div>

                            <div class="controlador input-field col s12 required">
                            </div>
                            
                            <div class="row">
                                <div class="input-field col s6">
                                    <a href="telaGerenciarMaterial.php" class="waves-effect waves-light btn red" style="width: 100%;">Cancelar</a>
                                </div>
                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">Enviar</button>                            
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
