<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});


if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <title>Disciplinas Pendentes</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   
    </head>    
    <script>
        $(document).ready(function () {
            $(".button-collapse").sideNav();
            $('.modal').modal();
        });
    </script>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" class="brand-logo center" style="font-family: 'Indie Flower';">Disciplinas em pendência</a>
                <ul>
                    <li><a href="telaGerenciarDisciplinas.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
            </div>
        </nav>
        <br><br>

        <?php
        if (!empty(DisciplinaController::mostrarTodasDisciplinasPendentes())):
            foreach (DisciplinaController::mostrarTodasDisciplinasPendentes() as $value):
                ?>
                <div class="row">
                    <div class="card-panel col s8 offset-s2">
                        <div class="card-content horizontal">
                            <div class="row">
                                <div class="col s12 l6 black-text">
                                    <p class="flow-text center-align"><b>Nome da Disciplina:</b><br> <?php echo $value[1]; ?></p>
                                </div>
                                <div class="col s12 l6 black-text">
                                    <p class="flow-text  center-align"><b>Nome do Professor:</b><br> <?php echo $value[2]; ?></p>
                                </div>

                                <div class="row">
                                    <div class="input-field col s6">
                                        <form method="POST" action="../controller/DisciplinaController.php">
                                            <input type="hidden" name="excluirPendencia" value="<?php echo $value[0]; ?>">
                                            <button class="btn waves-effect waves-light red" type="submit" style="width: 100%;">REJEITAR</button>                                  
                                        </form>
                                    </div>
                                    <div class="input-field col s6">
                                        <form method="POST" action="../controller/DisciplinaController.php">
                                            <input type="hidden" name="cadastrar" value="<?php echo $value[0]; ?>">
                                            <input type="hidden" name="id" value="<?php echo $value[0]; ?>">
                                            <input type="hidden" name="nomeDisciplina" value="<?php echo $value[1]; ?>">
                                            <button class="btn waves-effect waves-light" type="submit" style="width: 100%;">CADASTRAR</button>                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </body>
</html>
