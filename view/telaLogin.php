<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

session_start();
?>

<html>
    <head>
        <title>Entrando no Monitorif</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   

    </head>
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
        });
    </script>

    <body class="indigo lighten-5">
        <br>
        <div class="row">
            <div class="col s6 m6 l4 offset-s3 offset-m3 offset-l4 card-panel">
                <form class="login-form col m12" method="post" action="../controller/Autenticacao.php">
                    <div class="row">
                        <div class="input-field col s12 center">
                            <a href="../index.php"><img src="imagens/Monitorif5.PNG" alt="" class="circle responsive-img valign profile-image-login" style="width: 60%; height: 30%;"></a>
                            <p class="center login-form-text">Monitoria a distância</p>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="material-icons prefix pt-5">person_outline</i>

                            <?php if (isset($_SESSION["matriculaError"])): ?>
                                <input type="text" name="matricula" id="matricula" value="<?php echo $_SESSION["matriculaError"]; ?>">
                            <?php else: ?>
                                <input type="text" name="matricula" id="matricula">
                            <?php endif; ?>
                            <label for="matricula" class="center-align" data-error="wrong" data-success="right">
                                Matrícula
                            </label> 
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <i class="material-icons prefix pt-5">lock_outline</i>
                            <input type="password"  name="senha" id="senha">
                            <label class="center-align" for="senha" data-error="wrong" data-success="right">
                                Senha
                            </label> 

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn waves-effect waves-light col s12">Entrar</button>                           
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <?php
    if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
        echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você digitou errado sua senha!', 'error');</script>";
        $_SESSION['sweet'] = null;
    endif;
    ?>

</html>
