<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});


if (!session_id()) {
    session_start();
}


if (!isset($_SESSION["user"]) || (isset($_SESSION['monitor']) and $_SESSION['monitor'] != 1)) {
//echo $_SESSION['monitor'];
    session_destroy();
    header("location: telaLogin.php");
}

$_SESSION['sweet'] = null;
$_SESSION['monitor'] = 1;
$var = unserialize($_SESSION['user']);
$_SESSION['matricula'] = $var->getMatricula();
$_SESSION["foto"] = $var->getFoto() . '&token=' . $var->getToken();
?>


<html>
    <head>
        <title>Monitor do Monitorif</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="../../vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>   

    </head>    
    <script>
        $(document).ready(function () {
            $(".button-collapse").sideNav();
            $('.modal').modal();

            $("input.autocomplete").autocomplete({
                data: {
<?php
if (!empty(MaterialController::mostrarTodosMateriais())):
    foreach (MaterialController::mostrarTodosMateriais() as $valor) :
        echo "'$valor[5]' : null,";
    endforeach;
endif;
?>
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });

            $("#pesquisa").focus(function () {
                $(this).keydown(function () {
                    $.get("../controller/recebeAutoComplete.php", {"valor": $("#pesquisa").val()}, function (data) {
                        $("#card").html(data);
                    });
                });
            });

            var estado;
            $(".curtir").click(function () {
                if ($(this).attr("title") === "curtir" || estado === "curtir") {
                    var ident = parseInt($(this).attr("alt"));
                    $(this).children().css("color", "blue");
                    estado = $(this).attr("title", "descurtir");
                    $(".total").each(function (idx, el) {
                        if (idx === ident) {
                            $(el).text(parseInt($(el).text()) + 1 + " pessoas curtiram este material");

                        }
                    })

                    $.post("../controller/MaterialController.php",
                            {
                                curtida: "curtida",
                                idMaterial: $(this).attr("name"),
                            }, function (data, status) {
                        //alert(data, status)

                    });
                } else if ($(this).attr("title") === "descurtir" || estado === "descurtir") {
                    var ident = parseInt($(this).attr("alt"));
                    $(this).children().css("color", "black");
                    estado = $(this).attr("title", "curtir");
                    $(".total").each(function (idx, el) {
                        if (idx === ident) {
                            $(el).text(parseInt($(el).text()) - 1 + " pessoas curtiram este material");

                        }
                    })
                    $.post("../controller/MaterialController.php",
                            {
                                descurtida: "descurtida",
                                idMaterial: $(this).attr("name"),
                            }, function (data, status) {

                    });

                }
            });

            $(".comentario").click(function () {
                var ident = parseInt($(this).attr("name"));
                var nome = $(this).attr("alt");
                var count = 0;
                $(".materialize-textarea").each(function (idx, el) {
                    if (idx === ident) {
                        var texto = $(el).val();
                        $.post("../controller/ComentarioController.php",
                                {
                                    comentario: "comentario",
                                    idMaterial: $(el).attr("name"),
                                    texto: texto,
                                    nome: nome,
                                    matricula: <?php echo $_SESSION["matricula"]; ?>
                                }, function (data, status) {
                            $(el).val(" ");
                            swal('Obrigado por deixar a sua opinião aqui', ' ', 'success');
                            $(".novoComentario").each(function (i, e) {
                                if (ident === i) {
                                    $(e).after("<div class='aux-coment indigo lighten-5'><p><span class='blue-text'> " + nome + ": </span> " + texto + "</p></div><br>");
                                }
                            });
                        });
                    }
                    count++;
                });
            });


            $('.materias').click(function () {
                var nomeDisciplina = $(this).attr('name');
                $('.pesquisaDisciplina').hide();
                $('.pesquisaDisciplina').each(function (idx, el) {
                    if (nomeDisciplina === $(el).attr("name")) {
                        $(el).show();
                    }
                });
            });
        });

    </script>
    <style>
        .aux-coment{
            border-radius:3px;
            border: 1px solid;
            border-color: white;
        }
        .side-nav{
            overflow-y: hidden;
        }

        .side-nav:hover{
            overflow-y: auto;
        }

    </style>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

                <ul class="right">
                    <li>    
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field indigo lighten-5"  style="border-radius:3px; height: 70%; margin-top: 4%;">
                                        <i class="teal-text material-icons prefix" style="margin-top:-4%;">search</i>
                                        <input id="pesquisa" type="text" placeholder="search" id="autocomplete-input" class="autocomplete black-text" >
                                    </div>
                                </div>
                            </div>
                        </div>          
                    </li>
                    <div class="hide-on-med-and-down right">
                        <li style="margin-left: 4%;"><a href="telaGerenciarTicket.php" class="waves-effect waves-light btn"><i class="material-icons left">sms</i>Gerenciar Ticket</a></li>
                        <li><a href="telaGerenciarMaterial.php" class="waves-effect waves-light btn"><i class="material-icons left">create</i>Gerenciar Material</a></li>
                        <li><a href="telaPendencias.php" class="waves-effect waves-block waves-light notification-button"><i class="material-icons">notifications_none</i></a></li>
                        <li style="margin-left: -4%;">
                            <small class="notification-badge">
                                <span class="new badge">
                                    <?php
                                    if (empty(TicketController::mostrarTodosTickets())) {
                                        echo 0;
                                    } else {
                                        echo count(TicketController::mostrarTodosTickets());
                                    }
                                    ?>
                                </span>
                            </small>
                        </li>                    
                        <li><a href="../controller/sessionDestroy.php"><img src="../view/imagens/Logout_37127.png"  class="responsive-img" style="width: 35px; height: 30px;; margin: 50% auto;"></a></li>
                    </div>
                </ul>

                <ul class="side-nav" id="mobile-demo">
                    <li class="user-details teal lighteen-2" >
                        <div class="row">
                            <div class="col s4">
                                <img src="<?= $var->getFoto() . '&token=' . $var->getToken() ?>"  class="circle responsive-img valign profile-image cyan" style="margin-top: 15%; height: 10%;">
                            </div>
                            <div class="col s8">

                                <?php
                                foreach (MonitorController::mostrarTodosMonitores() as $value):
                                    if ($value[1] == $_SESSION['matricula']) {
                                        echo $value[0];
                                    }
                                endforeach;
                                ?>
                                <i class="mdi-navigation-arrow-drop-down right"></i></a>
                                <p class="user-roal white-text">Monitor de 
                                    <?php
                                    foreach (MonitorController::mostrarTodosMonitores() as $value):
                                        if ($value[1] == $_SESSION['matricula']) {
                                            $nomeDisciplina = DisciplinaController::procurarDisciplina($value[1]);
                                            foreach ($nomeDisciplina as $disciplina) {
                                                echo $disciplina[1] . "<br>";
                                            }
                                            //procurarDisciplinaQueLeciona($value[0]);
                                        }
                                    endforeach;
                                    ?>

                                </p>

                            </div>
                        </div>
                    </li>
                    <li class="bold">
                        <a a href="telaGerenciarTicket.php" class="collapsible-header waves-effect waves-cyan">
                            <i class="material-icons left">sms</i>
                            <span class="nav-text">Gerenciar Ticket</span>
                        </a>
                    </li>

                    <li class="bold">
                        <a href="telaGerenciarMaterial.php" class="collapsible-header waves-effect waves-cyan">
                            <i class="material-icons left">create</i>
                            <span class="nav-text">Gerenciar Material</span>
                        </a>
                    </li>

                    <li class="bold">
                        <a href="telaPendencias.php" class="collapsible-header waves-effect waves-cyan">
                            <i class="material-icons">notifications_none</i>
                            <small class="notification-badge" style="margin-left: -17%;">
                                <span class="new badge">
                                    <?php
                                    if (empty(TicketController::mostrarTodosTickets())) {
                                        echo 0;
                                    } else {
                                        echo count(TicketController::mostrarTodosTickets());
                                    }
                                    ?>
                                </span>
                            </small>
                            <span>Suas pendências</span>
                        </a>
                    </li>
                    <li><a href="../controller/sessionDestroy.php" class="collapsible-header waves-effect waves-cyan">Logout<i class="material-icons left">keyboard_return</i></a></li>
                    <hr>

                    <?php
                    $array = DisciplinaController::mostrarTodasDisciplinas();
                    if (isset($array) && !empty($array)) {

                        foreach ($array as $value) {
                            echo "<div class='materias' name='$value[1]'>
                        <li class='bold'>
                        <a class='collapsible-header waves-effect waves-cyan center'>
                        <span class='nav-text'>$value[1]</span>
                        </a>
                        </li>
                        </div>";
                        }
                    }
                    ?>
                    <div>
                        <li class="bold">
                            <a class="collapsible-header  waves-effect waves-cyan">
                                <span class="nav-text" style="visibility: hidden;">ocupar espaco</span>
                            </a>
                        </li>
                    </div>
                </ul>
            </div>
        </nav> 
        <br><br>
        <ul class="side-nav fixed">
            <li class="user-details teal lighteen-2" >
                <div class="row">
                    <div class="col s4">
                        <img src="<?= $var->getFoto() . '&token=' . $var->getToken() ?>"  class="circle responsive-img valign profile-image cyan" style="margin-top: 15%; height: 10%;">
                    </div>
                    <div class="col s8">

                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav">
                            <?php
                            foreach (MonitorController::mostrarTodosMonitores() as $value):
                                if ($value[1] == $_SESSION['matricula']) {
                                    echo $value[0];
                                }
                            endforeach;
                            ?>
                            <i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal white-text">Monitor de: <br>
                            <?php
                            foreach (MonitorController::mostrarTodosMonitores() as $value):
                                if ($value[1] == $_SESSION['matricula']) {
                                    $nomeDisciplina = DisciplinaController::procurarDisciplina($value[1]);
                                    foreach ($nomeDisciplina as $disciplina) {
                                        echo $disciplina[1] . "<br>";
                                    }
                                    //procurarDisciplinaQueLeciona($value[0]);
                                }
                            endforeach;
                            ?>
                        </p>

                    </div>

                </div>
            </li>


            <?php
            if (isset($array) && !empty($array)) {
                foreach ($array as $value) {
                    echo "<div class='materias' name='$value[1]'>
                        <li class='bold'>
                        <a class='collapsible-header waves-effect waves-cyan center'>
                        <span class='nav-text'>$value[1]</span>
                        </a>
                        </li>
                        </div>";
                }
            }
            ?>
        </ul>

        <div id="card">
            <?php
            $count = 0;
            if (!empty(MaterialController::mostrarTodosMateriais())):
                foreach (MaterialController::mostrarTodosMateriais() as $value):
                    ?>
                    <div class="pesquisaDisciplina" name=<?php echo $value[6]; ?>>
                        <div class="row">
                            <div class="offset-s3 offset-m4 col s8 m6">
                                <ul class="card">
                                    <li class="blue-grey">
                                        <div class="row" style="padding: 10px">
                                            <div class="col s3">
                                                <img src="<?php echo $value[1]; ?>"  class="circle responsive-img valign profile-image cyan">
                                            </div>
                                            <div>
                                                <a class="col s5 btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav"><?php echo $value[3]; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                                                <?php //<span class="col s4 activator white-text btn-flat"><i class="material-icons right">more_vert</i></span>
                                                ?><p class="col s7 user-roal white-text"><?php echo $value[6]; ?></p>
                                            </div>   
                                        </div>
                                    </li>
                                    <li style="margin-left: 10px;"><p style="font-size: 20px;"><?php echo $value[5]; ?></p></li>

                                    <li>
                                        <p class="center" style="font-size: 20px;"><?php echo $value[4]; ?></p>
                                    </li>
                                    <li class="row">
                                        <p class="center-align" style="font-size: 20px;">R - <?php echo $value[2]; ?></p>
                                    </li>


                                    <div class="row center">
                                        <?php
                                        $array = ArquivoMaterialController::loadArquivoMaterial($value[0]);
                                        if (!empty($array)):
                                            foreach ($array as $valor) :
                                                if ($valor[2] == "image/gif" || $valor[2] == "image/jpeg" || $valor[2] == "image/jpg" || $valor[2] == "image/png") {
                                                    ?> <a class="modal-trigger" href="#modal2<?php echo $valor[0]; ?>"><img src="../upload/material/<?php echo $valor[1]; ?>" class="responsive-img" style="width: 50%; height: 50%;"></a>
                                                    <?php
                                                } elseif ($valor[2] == "video/mp4") {
                                                    ?> <video class="responsive-video" controls><source src="../upload/material/<?php echo $valor[1]; ?>" type="video/mp4"></video>
                                                    <?php } else { ?>
                                                    <a href="../upload/material/<?php echo $valor[1]; ?>"> <?php echo $valor[4]; ?> </a>
                                                <?php } ?>
                                                <div id="modal2<?php echo $valor[0]; ?>" class="modal">
                                                    <div class="modal-content">
                                                        <img src="../upload/material/<?php echo $valor[1]; ?>" class="responsive-img">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                                    </div>
                                                </div>

                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </div>


                                    <li style="margin-left: 10px; margin-right: 10px;"> 

                                        <?php
                                        $links = MaterialController::mostrarLinks($value[0]);
                                        if (isset($links)):
                                            echo "<p style='font-size:18px;'>Links externos:</p>";
                                            foreach ($links as $val) :
                                                ?>
                                                <a href="<?php echo $val[1] ?> "><?php echo $val[1]; ?></a><br>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </li>
                                    <div class="divider"></div>
                                    <li class="row" style="margin-top: 2%;">
                                        <?php
                                        if (MaterialController::possuiCurtida($value[0], $_SESSION["matricula"])):
                                            ?>
                                            <div name="<?php echo $value[0]; ?>" class="curtir" title="descurtir" alt="<?php echo $count; ?>">
                                                <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                                    <i class="material-icons" btn-flat blue-text>thumb_up</i>
                                                </a><span class=" col s2" style="margin-left: -4%; color: blue">Curtir</span>
                                            </div>
                                        <?php else: ?>
                                            <div name="<?php echo $value[0]; ?>" class="curtir" title="curtir" alt="<?php echo $count; ?>">
                                                <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                                    <i class="material-icons" btn-flat grey-text>thumb_up</i>
                                                </a><span class="col s2" style="margin-left: -4%;">Curtir</span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="activator">
                                            <a class="col s3 m2 l1 waves-effect waves-block waves-light grey-text">
                                                <i class="material-icons">sms</i>
                                            </a><span class="col s3 m2 l1" style="margin-left: -4%;">Comentar</span>
                                        </div>
                                    </li>
                                    <li style="margin-top: -2%; margin-left: 2%; font-size: 15px;" class="grey-text">
                                        <div  class="total" name="<?php echo $value[0]; ?>">
                                            <span> <?php echo MaterialController::mostraQuantidadeCurtidasPorMaterial($value[0]); ?>
                                            </span> pessoas curtiram este material
                                        </div>
                                    </li>
                                    <div class="card-reveal blue-grey">
                                        <span class="card-title grey-text white-text">Faça seu comentário<i class="material-icons right">close</i></span>
                                        <br>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea placeholder="Comente aqui" class="materialize-textarea indigo lighten-5" name="<?php echo $value[0]; ?>" style="border-radius:3px; border: 1px solid; border-color: white;"></textarea>
                                            </div>
                                            <button class="btn right waves-effect waves-light comentario" name="<?php echo $count; ?>" alt="<?php echo $var->getNome(); ?>" type="submit">Enviar
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>

                                        <?php
                                        $comentarios = ComentarioController::mostraTodosComentarios($value[0]);
                                        if (isset($comentarios)):
                                            ?><span class="grey-text white-text" style="font-size: 20px;">
                                                comentários:
                                            </span><br><br>
                                            <div class="novoComentario indigo lighten-5" name="<?php echo $count; ?>">
                                            </div>
                                            <?php
                                            foreach ($comentarios as $v):
                                                ?>
                                                <div class="indigo lighten-5" style="border-radius:3px; border: 1px solid; border-color: white;">
                                                    <p><span class="blue-text"><?php echo $v[4] . " "; ?>:</span> <?php echo $v[1]; ?></p>
                                                </div><br>
                                                <?php
                                            endforeach;
                                        else:
                                            ?>
                                            <span class = "grey-text white-text" style = "font-size: 20px;">
                                                Seja o primeiro a comentar
                                            </span><br><br>
                                            <div class="novoComentario indigo lighten-5" name="<?php echo $count; ?>">
                                            </div>
                                        <?php
                                        endif;
                                        ?>

                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                    $count++;
                endforeach;
            endif;
            ?>
        </div>

    </body>
</html>
