<?php
require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
?>

<?php
if (!session_id()) {
    session_start();
}
?>


<html>
    <head>
        <title>Observando Monitores</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../static/materialize/css/materialize.min.css">
        <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet'> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script type="text/javascript" src="../static/js/jquery-3.3.1.min.js"></script>
        <script src="../static/materialize/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>    
    <script>
        $(document).ready(function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            $('.modal').modal();

        });
    </script>

    <?php
    $array = ProfessorController::buscarDisciplinasDoProfessor($_SESSION["matricula"]);
    if (!empty($array)) {

        $contMonitor = 0;
        foreach ($array as $valor) {

            $vetor = MonitorController::mostrarMonitoresPorDisciplina($valor[0]);
            if (!empty($vetor)) {
                if (!isset($monitores)) {
                    $contMonitor++;
                    $monitores = MonitorController::mostrarMonitoresPorDisciplina($valor[0]);
                } else {
                    $contMonitor++;
                    $monitores = array_merge($monitores, $vetor);
                }
            }
        }
    } else {
        $monitores = null;
    }
    ?>

    <body  style="background:#f0f0f0;">
        <nav>
            <div class="nav-wrapper indigo lighten-2">
                <a data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="hide-on-med-and-down right">
                    <li><a href="telaCadastrarMonitor.php" class="waves-effect waves-light btn"><i class="material-icons left">person_add</i>Cadastrar Monitor</a></li>
                </ul>
                <ul class="right">
                    <?php
                    if ($contMonitor > 0) {
                        ?>
                        <li><a href="telaRelatorios.php" class="waves-effect waves-light btn"><i class="material-icons left">library_add</i>Ver Relatórios</a></li>
                    <?php }
                    ?>
                </ul>
                <ul class="hide-on-med-and-down">
                    <li><a href="telaProfessor.php"><i class="material-icons left">keyboard_return</i></a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="telaCadastrarMonitor.php">Cadastrar Monitor</a></li>
                    <li><a href="telaProfessor.php">Voltar para tela inicial</i></a></li>
                </ul>

            </div>
        </nav>
        <br><br>
        <h3 class="center"> SEUS MONITORES </h3><br>


        <?php
        if (!empty($monitores)):
            ?>
            <div class="row">
                <div class="col s8 offset-s2 white">
                    <table class="centered responsive-table bordered white">
                        <thead>
                            <tr>
                                <th>Nome Completo</th>
                                <th>Matricula</th>
                                <th>Disciplina</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($monitores as $value):
                                ?>
                                <tr>
                                    <td><?php echo $value[0]; ?></td>
                                    <td><?php echo $value[1]; ?></td>
                                    <td>
                                        <?php
                                        $array = MonitorController::mostrarDisciplinasDeCadaMonitor($value[2]);
                                        foreach ($array as $valor) {
                                            if ($valor == $array[count($array) - 1]) {
                                                echo $valor[1];
                                            } else {
                                                echo $valor[1] . " / ";
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <form method="POST" action="telaEditandoMonitor.php" >
                                            <a href="#modal2<?php echo $value[1]; ?>" class="red-text modal-trigger"><i class='material-icons'>delete</i></a>
                                            <input type="hidden" name="idEditar" value="<?php echo $value[1]; ?>" />
                                            <a class="green-text btn-flat" onclick='this.parentNode.submit(); return false;' style="margin-top: -5%;"><i class="material-icons">edit</i></a>
                                            <a class="blue-text modal-trigger" href="#modal1<?php echo $value[1]; ?>"><i class='material-icons'>visibility</i></a>
                                        </form>

                                        <div id="modal1<?php echo $value[1]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Informações do Ticket</h5>
                                                <p><b>Nome do monitor:</b> <?php echo $value[0]; ?></p>
                                                <p><b>Matrícula do monitor:</b> <?php echo $value[1]; ?></p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#!" class="modal-close waves-effect waves-green btn">FECHAR</a>
                                            </div>
                                        </div>

                                        <div id="modal2<?php echo $value[1]; ?>" class="modal">
                                            <div class="modal-content">
                                                <h5>Você tem certeza que deseja remover esse monitor?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <form method="POST" action="../controller/MonitorController.php" >
                                                    <input type="hidden" name="idExcluir" value="<?php echo $value[1]; ?>"/>
                                                    <a href="#!" class="modal-close waves-effect waves-green btn red">Cancelar</a>
                                                    <button class="modal-close waves-effect waves-green btn" type="submit">Confirmar</button>    
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                    </table>
                </div>
            </div>
            <?php
        endif;
        if (!empty($_SESSION['sweet'])):
            echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você editou um monitor!', 'success');</script>";
            $_SESSION['sweet'] = null;
        endif;
        ?>
    </body>
</html>
