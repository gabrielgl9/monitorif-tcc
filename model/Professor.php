<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});
if (!session_id()) {
    session_start();
}

class Professor {

    private $matricula;
    private $nome;

    public function verificaSePossuiDisciplina($idDisciplina, $matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM professor, professor_disciplina, disciplina WHERE "
                . "professor.matricula = professor_disciplina.matricula_professor AND "
                . "professor_disciplina.id_disciplina = disciplina.id_disciplina AND "
                . "professor.matricula = ? AND disciplina.id_disciplina = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->bindParam(2, $idDisciplina);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results = "entrou";
        }

        $pdo->desconecta($conn);
        if (isset($results)) {
            return true;
        } else {
            return false;
        }
    }

    public function deletarDisciplinasPorProfessor($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM professor_disciplina WHERE matricula_professor = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function adicionarDisciplina($idDisciplina, $matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO professor_disciplina (matricula_professor, id_disciplina) VALUES (? ,?)");
        $stmt->bindParam(1, $matricula);
        $stmt->bindParam(2, $idDisciplina);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function buscarDisciplinasDoProfessor($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT DISTINCT * FROM professor, professor_disciplina, disciplina WHERE "
                . "professor.matricula = professor_disciplina.matricula_professor AND "
                . "professor_disciplina.id_disciplina = disciplina.id_disciplina AND "
                . "professor.matricula = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = array($row->id_disciplina, $row->nome);
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function verificarSeExisteProfessor($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM professor WHERE matricula = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = array($row->matricula, $row->nome_professor);
        }

        $pdo->desconecta($conn);

        if (isset($results)) {
            return true;
        } else {
            return false;
        }
    }

    public function cadastrarProfessor($matricula, $nome) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO professor (matricula, nome_professor) VALUES (? ,?)");
        $stmt->bindParam(1, $matricula);
        $stmt->bindParam(2, $nome);
        $stmt->execute();

        $pdo->desconecta($conn);
        return false;
    }

}

?>