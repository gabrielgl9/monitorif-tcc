<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class ArquivoTicket {

    private $nome;
    private $nomeVerdadeiro;
    private $tipo;
    private $codigoTicket;

    function getNome() {
        return $this->nome;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getCodigoTicket() {
        return $this->codigoTicket;
    }

    function getNomeVerdadeiro() {
        return $this->nomeVerdadeiro;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setCodigoTicket($codigoTicket) {
        $this->codigoTicket = $codigoTicket;
    }

    function setNomeVerdadeiro($nomeVerdadeiro) {
        $this->nomeVerdadeiro = $nomeVerdadeiro;
    }

    public function inserirArquivo() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO arquivo_ticket (name, type, id_ticket, nome_verdadeiro) VALUES (?, ?, ?, ?)");
        $stmt->bindparam(1, $this->nome);
        $stmt->bindparam(2, $this->tipo);
        $stmt->bindparam(3, $this->codigoTicket);
        $stmt->bindParam(4, $this->nomeVerdadeiro);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function excluirArquivoPorTicket() {


        $arr = self::carregarArquivoPorTicket();
        if (!empty($arr)) {
            foreach ($arr as $value) {
                unlink("../upload/ticket/" . $value[1]);
            }
        }

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM arquivo_ticket WHERE id_ticket = ?");
        $stmt->bindparam(1, $this->codigoTicket);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function excluirArquivoPorId($id) {

        $arr = self::carregarArquivoPorId($id);
        if (!empty($arr)) {
            foreach ($arr as $value) {
                unlink("../upload/ticket/" . $value[1]);
            }
        }

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM arquivo_ticket WHERE id_arquivo_ticket = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function carregarArquivoPorId($id) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM arquivo_ticket WHERE id_arquivo_ticket = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_arquivo_ticket, $row->name, $row->type, $row->id_ticket, $row->nome_verdadeiro);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function carregarArquivoPorTicket() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM arquivo_ticket WHERE id_ticket = ?");
        $stmt->bindparam(1, $this->codigoTicket);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_arquivo_ticket, $row->name, $row->type, $row->id_ticket, $row->nome_verdadeiro);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
