<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Ticket {

    public static $id;
    private $descricao;
    private $conteudo;
    private $disciplina;
    private $pendencia;

    function getDescricao() {
        return $this->descricao;
    }

    function getConteudo() {
        return $this->conteudo;
    }

    function getDisciplina() {
        return $this->disciplina;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setConteudo($conteudo) {
        $this->conteudo = $conteudo;
    }

    function setDisciplina($disciplina) {
        $this->disciplina = $disciplina;
    }

    public function inserirNovoTicket() {

        $pdo = new Conexao();
        $con = $pdo->conecta();
        $stmt = $con->prepare("INSERT INTO ticket (descricao, conteudo, disciplina, pendencia, matricula_aluno, data_ticket) VALUES (?,?,?,1,?,?)");
        $stmt->bindParam(1, $this->descricao);
        $stmt->bindParam(2, $this->conteudo);
        $stmt->bindParam(3, $this->disciplina);
        $stmt->bindParam(4, $_SESSION['matricula']);
        $stmt->bindParam(5, date('Y-m-d'));
        $stmt->execute();
        $lastId = $con->lastInsertId();
        $pdo->desconecta($con);

        $pdo = new Conexao();
        $con = $pdo->conecta();
        $stmt = $con->prepare("SELECT * FROM monitor, monitor_disciplina, disciplina WHERE monitor.id_monitor = monitor_disciplina.id_monitor and monitor_disciplina.id_disciplina = disciplina.id_disciplina and disciplina.nome = ?");
        $stmt->bindParam(1, $this->disciplina);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_monitor);
            }
        }
        $pdo->desconecta($con);

        if (!empty($results)) {
            foreach ($results as $value) {

                $pdo = new Conexao();
                $con = $pdo->conecta();
                $stmt = $con->prepare("INSERT INTO ticket_monitor (id_ticket, id_monitor) VALUES (?,?)");
                $stmt->bindParam(1, $lastId);
                $stmt->bindParam(2, $value[0]);
                $stmt->execute();
                $pdo->desconecta($con);
            }
        } else {
            $pdo = new Conexao();
            $con = $pdo->conecta();
            $stmt = $con->prepare("DELETE FROM ticket WHERE id_ticket = ?");
            $stmt->bindParam(1, $lastId);
            $stmt->execute();
            $pdo->desconecta($con);
            $lastId = -5;
        }

        return $lastId;
    }

    public function ativarPendencia($codTicket) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE ticket SET pendencia = 1 WHERE id_ticket = ?");
        $stmt->bindparam(1, $codTicket);
        $stmt->execute();
        $pdo->desconecta($conn);

        return true;
    }

    public function desativarPendencia($codTicket) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE ticket SET pendencia = 0 WHERE id_ticket = ?");
        $stmt->bindparam(1, $codTicket);
        $stmt->execute();
        $pdo->desconecta($conn);

        return true;
    }

    public function mostrarSeusTickets() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM ticket where matricula_aluno = ? AND pendencia = 1");
        $stmt->bindParam(1, $_SESSION['matricula']);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_ticket, $row->descricao, $row->conteudo, $row->disciplina);
            }
        }

        $pdo->desconecta($con);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarTodosTicketsAtivos() {

        $pdo = new Conexao();
        $con = $pdo->conecta();
        $stmt = $con->prepare("SELECT * FROM ticket, ticket_monitor, monitor "
                . "WHERE ticket.id_ticket = ticket_monitor.id_ticket "
                . "AND ticket_monitor.id_monitor = monitor.id_monitor "
                . "AND monitor.matricula = ? AND ticket.pendencia = 0");

        $stmt->bindParam(1, $_SESSION['matricula']);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_ticket, $row->descricao, $row->conteudo);
            }
        }
        $pdo->desconecta($con);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarTodosTicketsPendentes() {

        $pdo = new Conexao();
        $con = $pdo->conecta();
        $stmt = $con->prepare("SELECT * FROM ticket, ticket_monitor, monitor "
                . "WHERE ticket.id_ticket = ticket_monitor.id_ticket "
                . "AND ticket_monitor.id_monitor = monitor.id_monitor "
                . "AND monitor.matricula = ? AND ticket.pendencia = 1");

        $stmt->bindParam(1, $_SESSION['matricula']);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_ticket, $row->descricao, $row->conteudo);
            }
        }
        $pdo->desconecta($con);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function deletarTicketPendente($id) {


        ArquivoTicketController::excluirArquivoPorTicket($id);

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM ticket WHERE id_ticket = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();
        $pdo->desconecta($conn);

        $conn2 = $pdo->conecta();
        $stmt2 = $conn2->prepare("DELETE FROM ticket_monitor WHERE id_ticket = ?");
        $stmt2->bindparam(1, $id);
        $stmt2->execute();
        $pdo->desconecta($conn2);

        return true;
    }

    public function modificarTicket($id) {

        $pdo = new Conexao();

        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE ticket set descricao = ?, conteudo = ? , disciplina = ? where id_ticket = ? ");
        $stmt->bindparam(1, $this->descricao);
        $stmt->bindparam(2, $this->conteudo);
        $stmt->bindparam(3, $this->disciplina);
        $stmt->bindparam(4, $id);
        $stmt->execute();
        $pdo->desconecta($conn);

        $conn2 = $pdo->conecta();
        $stmt2 = $conn2->prepare("DELETE FROM ticket_monitor WHERE id_ticket = ?");
        $stmt2->bindparam(1, $id);
        $stmt2->execute();
        $pdo->desconecta($conn2);

        foreach (self::retornaIdPorDisciplina($this->disciplina) as $value) {

            $conn3 = $pdo->conecta();
            $stmt3 = $conn3->prepare("INSERT INTO ticket_monitor (id_ticket, id_monitor) VALUES (?, ?)");
            $stmt3->bindValue(1, $id);
            $stmt3->bindValue(2, $value[0]);
            $stmt3->execute();
            $pdo->desconecta($conn3);
        }


        return true;
    }

    public function retornaIdPorDisciplina($disciplina) {

        $pdo = new Conexao();
        $con = $pdo->conecta();
        $stmt = $con->prepare("SELECT monitor.id_monitor 
                            FROM monitor, disciplina, monitor_disciplina 
                            WHERE monitor.id_monitor = monitor_disciplina.id_monitor 
                            AND monitor_disciplina.id_disciplina = disciplina.id_disciplina
                            AND disciplina.nome = ?");
        $stmt->bindparam(1, $disciplina);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_monitor);
            }
        }
        $pdo->desconecta($con);
        return $results;
    }

}
