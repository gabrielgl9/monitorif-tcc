<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Comentario {

    private $idMaterial;
    private $matricula;
    private $texto;
    private $nome;

    function getIdMaterial() {
        return $this->idMaterial;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getTexto() {
        return $this->texto;
    }

    function setIdMaterial($idMaterial) {
        $this->idMaterial = $idMaterial;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function getNome() {
        return $this->nome;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    public function insert() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO comentario (id_material, matricula, texto, nome) VALUES (?, ?, ?, ?)");
        $stmt->bindParam(1, $this->getIdMaterial());
        $stmt->bindParam(2, $this->getMatricula());
        $stmt->bindParam(3, $this->getTexto());
        $stmt->bindParam(4, $this->getNome());
        $stmt->execute();
        $pdo->desconecta($conn);
    }

    public function mostraQuantidadeComentarios($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT COUNT(*) as total FROM comentario WHERE id_material = ?");
        $stmt->bindParam(1, $idMaterial);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = $row->total;
            }
        }

        $pdo->desconecta($conn);

        return $results;
    }

    public function mostraTodosComentarios($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM comentario WHERE id_material = ?");
        $stmt->bindParam(1, $idMaterial);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_comentario, $row->texto, $row->id_material, $row->matricula, $row->nome);
            }
        }

        $pdo->desconecta($conn);

        if (isset($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
