<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class ArquivoMaterial {

    private $nome;
    private $nomeVerdadeiro;
    private $tipo;
    private $codigoMaterial;

    function getNome() {
        return $this->nome;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getCodigoMaterial() {
        return $this->codigoMaterial;
    }

    function getNomeVerdadeiro() {
        return $this->nomeVerdadeiro;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setCodigoMaterial($codigoMaterial) {
        $this->codigoMaterial = $codigoMaterial;
    }

    function setNomeVerdadeiro($nomeVerdadeiro) {
        $this->nomeVerdadeiro = $nomeVerdadeiro;
    }

    public function inserirArquivo() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO arquivo_material (name, type, id_material, nome_verdadeiro) VALUES (?, ?, ?, ?)");
        $stmt->bindparam(1, $this->nome);
        $stmt->bindparam(2, $this->tipo);
        $stmt->bindparam(3, $this->codigoMaterial);
        $stmt->bindParam(4, $this->nomeVerdadeiro);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function excluirArquivoPorMaterial() {

        $arr = self::carregarArquivoPorMaterial();
        if (!empty($arr)) {
            foreach ($arr as $value) {
                unlink("../upload/material/" . $value[1]);
            }
        }

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM arquivo_material WHERE id_material = ?");
        $stmt->bindparam(1, $this->codigoMaterial);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function excluirArquivoPorId($id) {

        $arr = self::carregarArquivoPorId($id);
        if (!empty($arr)) {
            foreach ($arr as $value) {
                unlink("../upload/material/" . $value[1]);
            }
        }

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM arquivo_material WHERE id_arquivo_material = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function carregarArquivoPorId($id) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM arquivo_material WHERE id_arquivo_material = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_arquivo_material, $row->name, $row->type, $row->id_material, $row->nome_verdadeiro);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function carregarArquivoPorMaterial() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM arquivo_material WHERE id_material = ?");
        $stmt->bindparam(1, $this->codigoMaterial);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_arquivo_material, $row->name, $row->type, $row->id_material, $row->nome_verdadeiro);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
