<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Monitor {

    private $nome;
    private $matricula;
    private $disciplina;

    function getNome() {
        return $this->nome;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getDisciplina() {
        return $this->disciplina;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setDisciplina($disciplina) {
        $this->disciplina = $disciplina;
    }

    public function CadastrarMonitor() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO monitor (matricula, nome) values (?,?)");
        $stmt->bindparam(1, $this->matricula);
        $stmt->bindparam(2, $this->nome);
        $stmt->execute();


        foreach ($this->mostrarTodosMonitores() as $value) {
            if ($value[1] == $this->matricula) {
                return $value[2];
            }
        }
    }

    public function deletarDisciplinaDoMonitor($matricula) {

        foreach ($this->mostrarTodosMonitores() as $value) {
            if ($value[1] == $matricula) {
                $idMonitor = $value[2];
            }
        }

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM monitor_disciplina WHERE id_monitor = ?");
        $stmt->bindparam(1, $idMonitor);
        $stmt->execute();
        $pdo->desconecta($conn);

        return $idMonitor;
    }

    public function inserirMonitorNaDisciplina($idDisciplina, $idMonitor) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO monitor_disciplina (id_monitor, id_disciplina) values (?,?)");
        $stmt->bindparam(1, $idMonitor);
        $stmt->bindparam(2, $idDisciplina);
        $stmt->execute();
        $pdo->desconecta($conn);
        return true;
    }

    public function mostrarTodosMonitores() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("select * from monitor");
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

                $results[] = array($row->nome, $row->matricula, $row->id_monitor);
            }
        }
        $pdo->desconecta($conn);

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarTodasDisciplinasDeCadaMonitor($idMonitor) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM monitor, monitor_disciplina, disciplina WHERE "
                . "monitor.id_monitor = monitor_disciplina.id_monitor AND "
                . "monitor_disciplina.id_disciplina = disciplina.id_disciplina AND "
                . "monitor.id_monitor = ?");
        $stmt->bindparam(1, $idMonitor);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

                $results[] = array($row->id_disciplina, $row->nome);
            }
        }
        $pdo->desconecta($conn);

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function editarMonitor($mat) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE monitor set nome = ?, matricula = ? where matricula = ? ");
        $stmt->bindparam(1, $this->nome);
        $stmt->bindparam(2, $this->matricula);
        $stmt->bindparam(3, $mat);
        $stmt->execute();
        $pdo->desconecta($conn);

        return true;
    }

    public function excluirMonitor($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM monitor WHERE matricula = ?");
        $stmt->bindparam(1, $matricula);
        $stmt->execute();
        $pdo->desconecta($conn);

        return true;
    }

    public function procurarDisciplina($idDisciplina, $matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT disciplina.nome
                                FROM monitor, monitor_disciplina, disciplina
                                WHERE monitor.matricula = ? AND disciplina.id_disciplina = ? AND
                                monitor.id_monitor = monitor_disciplina.id_monitor AND monitor_disciplina.id_disciplina = disciplina.id_disciplina");
        $stmt->bindparam(1, $matricula);
        $stmt->bindparam(2, $idDisciplina);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $disciplina = $row->nome;
            }
        }
        $pdo->desconecta($conn);
        if (isset($disciplina)) {
            return true;
        } else {
            return false;
        }
    }

    public static function mostrarMonitoresPorDisciplina($idDisciplina) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT monitor.nome, monitor.matricula, monitor.id_monitor "
                . "FROM monitor, monitor_disciplina, disciplina WHERE "
                . "monitor.id_monitor = monitor_disciplina.id_monitor AND "
                . "monitor_disciplina.id_disciplina = disciplina.id_disciplina AND "
                . "disciplina.id_disciplina = ?");
        $stmt->bindParam(1, $idDisciplina);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

                $results[] = array($row->nome, $row->matricula, $row->id_monitor);
            }
        }
        $pdo->desconecta($conn);

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
