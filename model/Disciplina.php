<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Disciplina {

    private $nome;
    private $id;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getNome() {
        return $this->nome;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    public function insert() {

        if (!empty($this->getId())) {
            $pdo = new Conexao();
            $conn = $pdo->conecta();
            $stmt = $conn->prepare("DELETE FROM disciplinas_pendentes WHERE id_disciplina_pendente = ?");
            $stmt->bindparam(1, $this->getId());
            $stmt->execute();
            $pdo->desconecta($conn);
        }
        $pdo2 = new Conexao();
        $conn2 = $pdo2->conecta();
        $stmt2 = $conn2->prepare("INSERT INTO disciplina (nome) VALUES (?)");
        $stmt2->bindparam(1, $this->nome);
        $stmt2->execute();
        $pdo2->desconecta($conn);
    }

    public function cadastrarDisciplinaPendente($nomeDisciplina, $nomeProfessor) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO disciplinas_pendentes (nome_disciplina, nome_professor) VALUES (?, ?)");
        $stmt->bindparam(1, $nomeDisciplina);
        $stmt->bindparam(2, $nomeProfessor);
        $stmt->execute();
        $pdo->desconecta($conn);
    }

    public function descontaDisciplinas($matricula) {

        foreach ($this->procurarDisciplinasPorMonitores($matricula) as $value) {

            $pdo = new Conexao();
            $conn = $pdo->conecta();
            $stmt = $conn->prepare("UPDATE disciplina SET num_monitor = (?)-1 WHERE id_disciplina = ?");
            $stmt->bindparam(1, $value[1]);
            $stmt->bindparam(2, $value[0]);
            $stmt->execute();
            $pdo->desconecta($conn);
        }
    }

    public function soma($id) {

        $x = 0;
        foreach ($this->getAll() as $value) {
            if ($value[0] == $id) {
                $cont = $value[2] + 1;
            }
            $x++;
        }
        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE disciplina SET num_monitor = ? WHERE id_disciplina = ? ");
        if ($x == 0) {
            $stmt->bindparam(1, 1);
        } else {
            $stmt->bindparam(1, $cont);
        }
        $stmt->bindparam(2, $id);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function pegarIdMonitorDeDisciplina($idDisciplina) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT id_monitor FROM monitor_disciplina WHERE id_disciplina = ?");
        $stmt->bindParam(1, $idDisciplina);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_monitor);
            }
        }
        $pdo->desconecta($conn);
        return $results;
    }

    public function pegarDisciplinasDeUmMonitor($idMonitor) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT disciplina.id_disciplina, disciplina.nome "
                . "FROM disciplina, monitor_disciplina, monitor WHERE disciplina.id_disciplina = monitor_disciplina.id_disciplina "
                . "AND monitor_disciplina.id_monitor = monitor.id_monitor AND monitor.id_monitor = ?");
        $stmt->bindParam(1, $idMonitor);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                echo "aaaaaaaaa";
                $results[] = array($row->id_disciplina, $row->nome);
            }
        }
        $pdo->desconecta($conn);

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function delete($id) {


        $monitor = $this->pegarIdMonitorDeDisciplina($id);

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM disciplina WHERE id_disciplina = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();
        $pdo->desconecta($conn);

        $pdo2 = new Conexao();
        $conn2 = $pdo2->conecta();
        $stmt2 = $conn2->prepare("DELETE FROM monitor_disciplina WHERE id_disciplina = ?");
        $stmt2->bindparam(1, $id);
        $stmt2->execute();
        $pdo2->desconecta($conn2);
        if (empty($this->pegarDisciplinasDeUmMonitor($monitor[0][0]))) {
            $pdo2 = new Conexao();
            $conn2 = $pdo2->conecta();
            $stmt2 = $conn2->prepare("DELETE FROM monitor WHERE id_monitor = ?");
            $stmt2->bindparam(1, $monitor[0][0]);
            $stmt2->execute();
            $pdo2->desconecta($conn2);
        }
        return true;
    }

    public function procurarDisciplina($id) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM monitor_disciplina, monitor, disciplina "
                . "WHERE monitor.matricula = ? AND monitor.id_monitor = monitor_disciplina.id_monitor "
                . "AND monitor_disciplina.id_disciplina = disciplina.id_disciplina");
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_disciplina, $row->nome, $row->nome, $row->num_monitor);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function procurarDisciplinasPorMonitores($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT disciplina.id_disciplina, disciplina.num_monitor 
                                FROM disciplina, monitor_disciplina, monitor
                                WHERE disciplina.id_disciplina = monitor_disciplina.id_disciplina AND 
                                monitor_disciplina.id_monitor = monitor.id_monitor AND monitor.matricula = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_disciplina, $row->num_monitor);
            }
        }

        $pdo->desconecta($conn);
        return $results;
    }

    public static function mostrarTodasDisciplinasPendentes() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM disciplinas_pendentes");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_disciplina_pendente, $row->nome_disciplina, $row->nome_professor);
            }
        }

        $pdo->desconecta($conn);
        if (isset($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarDisciplinasDoProfessor($matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM disciplina, professor_disciplina, professor WHERE "
                . "professor.matricula = professor_disciplina.matricula AND "
                . "professor_disciplina.id_disciplina = disciplina.id_disciplina AND "
                . "professor.matricula = ?");
        $stmt->bindParam(1, $matricula);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_disciplina, $row->nome, $row->num_monitor);
            }
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function getAll() {
        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM disciplina");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_disciplina, $row->nome, $row->num_monitor);
            }
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
