<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});


if (!session_id()) {
    session_start();
}

class Conexao {

    private $servidor = "localhost";
    private $database = "monitorif";
    private $user = "root";
    private $password = "";

    public function conecta() {

        $conn = null;
        $conn = new PDO("mysql:host=$this->servidor;dbname=$this->database", $this->user, $this->password);
        return $conn;
    }

    public function desconecta(&$conn) {
        $conn = null;
    }

}
