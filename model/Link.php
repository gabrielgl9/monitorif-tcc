<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Link {

    public function insert($url, $idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO link_material (url, id_material) VALUES (?, ?)");
        $stmt->bindParam(1, $url);
        $stmt->bindParam(2, $idMaterial);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function excluir($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM link_material WHERE id_material = ?");
        $stmt->bindparam(1, $idMaterial);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function mostrarLinks($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM link_material WHERE id_material = ?");
        $stmt->bindParam(1, $idMaterial);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_link_material, $row->url);
            }
        }

        $pdo->desconecta($conn);
        if (isset($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
