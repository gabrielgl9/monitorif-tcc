<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Aluno {

    protected $nome;
    protected $matricula;
    protected $foto;
    protected $idUsuario;
    protected $token;

    function getNome() {
        return $this->nome;
    }

    function getMatricula() {
        return $this->matricula;
    }

    function getFoto() {
        return $this->foto;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getToken() {
        return $this->token;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setToken($token) {
        $this->token = $token;
    }

}
