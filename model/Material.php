<?php

require_once '../controller/Autoloader.php';

spl_autoload_register(function ($className) {
    $import = new Autoloader();
    $import->register($className);
});

if (!session_id()) {
    session_start();
}

class Material {

    private $explicacao;
    private $codigoTicket;
    private $imagem;

    function getExplicacao() {
        return $this->explicacao;
    }

    function getCodigoTicket() {
        return $this->codigoTicket;
    }

    function setExplicacao($explicacao) {
        $this->explicacao = $explicacao;
    }

    function setCodigoTicket($codigoTicket) {
        $this->codigoTicket = $codigoTicket;
    }

    function getImagem() {
        return $this->imagem;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    public function cadastrarMaterial() {

        foreach (MonitorController::mostrarTodosMonitores() as $value):
            if ($value[1] == $_SESSION['matricula']) {
                $nomeMonitor = $value[0];
                $idMonitor = $value[2];
            }
        endforeach;
        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO material (foto, explicacao, id_ticket, id_monitor, data_material, nome_monitor) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bindparam(1, $this->imagem);
        $stmt->bindparam(2, $this->explicacao);
        $stmt->bindparam(3, $this->codigoTicket);
        $stmt->bindparam(4, $idMonitor);
        $stmt->bindParam(5, date('Y-m-d'));
        $stmt->bindparam(6, $nomeMonitor);
        $stmt->execute();
        $pdo->desconecta($conn);
    }

    public function excluirMaterial($id) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM material WHERE id_material = ?");
        $stmt->bindparam(1, $id);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function modificarMaterial($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("UPDATE material set explicacao = ? , id_ticket = ? where id_material = ? ");
        $stmt->bindparam(1, $this->explicacao);
        $stmt->bindparam(2, $this->codigoTicket);
        $stmt->bindparam(3, $idMaterial);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function verificaSeCurtiu($idMaterial, $matricula) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM curtida WHERE id_material = ? AND matricula = ?");
        $stmt->bindParam(1, $idMaterial);
        $stmt->bindParam(2, $matricula);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_material, $row->matricula);
            }
        }

        $pdo->desconecta($conn);
        if (isset($results)) {
            return true;
        } else {
            return false;
        }
    }

    public function removerCurtida($matricula, $idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("DELETE FROM curtida WHERE id_material = ? AND matricula = ?");
        $stmt->bindparam(1, $idMaterial);
        $stmt->bindParam(2, $matricula);
        $stmt->execute();

        $pdo->desconecta($conn);
        return true;
    }

    public function mostraQuantidadeCurtidasPorMaterial($idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT count(*) as total FROM curtida WHERE id_material = ?");
        $stmt->bindParam(1, $idMaterial);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = $row->total;
            }
        }

        $pdo->desconecta($conn);
        return $results;
    }

    public function mostrarTodosMateriaisDisponibilizados() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM ticket, material "
                . "WHERE ticket.id_ticket = material.id_ticket AND ticket.pendencia = 0 "
                . "order by id_material desc");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_material, $row->foto, $row->explicacao, $row->nome_monitor, $row->descricao, $row->conteudo, $row->disciplina, $row->id_ticket);
            }
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarSeusMateriais($idMonitor) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM material, ticket WHERE ticket.id_ticket = material.id_ticket "
                . "AND id_monitor = ?");
        $stmt->bindParam(1, $idMonitor);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_material, $row->foto, $row->explicacao, $row->descricao, $row->conteudo, $row->id_ticket);
            }
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function curtirMaterial($matricula, $idMaterial) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("INSERT INTO curtida (matricula, id_material) VALUES (?, ?)");
        $stmt->bindParam(1, $matricula);
        $stmt->bindParam(2, $idMaterial);
        $stmt->execute();

        $pdo->desconecta($conn);
    }

    public function autoComplete($conteudo) {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT * FROM ticket, material "
                . "WHERE ticket.id_ticket = material.id_ticket AND ticket.pendencia = 0 AND "
                . "upper(ticket.conteudo) like upper(?) order by id_material desc");
        $conteudo = "%" . $conteudo . "%";
        $stmt->bindparam(1, $conteudo);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_material, $row->foto, $row->explicacao, $row->nome_monitor, $row->descricao, $row->conteudo, $row->disciplina, $row->id_ticket);
            }
        }
        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostraMateriaisPorDisciplina() {

        $pdo = new Conexao();
        $conn = $pdo->conecta();
        $stmt = $conn->prepare("SELECT DISTINCT material.id_material, material.foto, material.explicacao, monitor.nome, 
                                ticket.descricao, ticket.conteudo, ticket.disciplina, DATE_FORMAT(data_ticket, '%d/%m/%Y') as data_ticket, DATE_FORMAT(data_material, '%d/%m/%Y') as data_material 
                                FROM material, ticket, monitor, monitor_disciplina, professor_disciplina, disciplina
                                WHERE monitor.id_monitor = material.id_monitor AND material.id_ticket = ticket.id_ticket AND monitor.id_monitor = monitor_disciplina.id_monitor AND monitor_disciplina.id_disciplina = professor_disciplina.id_disciplina AND professor_disciplina.matricula_professor = ? AND professor_disciplina.id_disciplina = disciplina.id_disciplina AND disciplina.nome = ticket.disciplina
                                order by material.id_material desc");
        $stmt->bindParam(1, $_SESSION["matricula"]);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->id_material, $row->foto, $row->explicacao, $row->nome, $row->descricao, $row->conteudo, $row->disciplina, $row->data_ticket, $row->data_material);
            }
        }

        $pdo->desconecta($conn);
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

}
