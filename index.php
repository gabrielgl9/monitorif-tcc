<?php
if (!session_id()) {
    session_start();
}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Bem Vindo ao Monitorif</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>    

    <script>
        $(document).ready(function () {
            $('a').click(function () {
                var pos = $('#' + this.dataset.id).position().top;
                $('html, body').animate({
                    scrollTop: pos
                }, 1000);
            });
        });
    </script>

    <body  class="grey lighten-5">

        <div class="row">
            <span class="left hide-on-med-and-down">  
                <img src="/view/imagens/Monitorif4.PNG" alt="" class="circle responsive-img valign profile-image-login" style="position: absolute;">
            </span>
            <div class="center">
                <a data-id="sobre" class="btn-flat" style="margin-right: 3%;">SOBRE</a>
                <a data-id="equipe" class="btn-flat" style="margin-right: 3%;">EQUIPE</a>
                <a href="view/telaLogin.php" class="waves-effect waves-light btn indigo"><i class="material-icons right">forward</i>ENTRAR</a>
            </div>
        </div>


        <div id="index-banner" >
            <div class="section no-pad-bot">
                <div class="container">
                    <br><br>
                    <h1 class="header center"><div class="teal-text">ENTRE NO MONITORIF</div></h1>
                    <div class="row center">
                        <h5 class="header col s12 light">Uma plataforma web para a monitoria a distância</h5>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>


        <div id="sobre">
            <span class="nav-text" style="visibility: hidden;">ocupar espaco</span>
        </div>

        <div class="section">
            <div class="row">

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center teal-text"><i class="material-icons large">group</i></h2>
                        <h5 class="center">Auxílio aos alunos em qualquer horário</h5>
                        <p class="light center" style="font-size: 16px;">Sabemos que muitos alunos realizam tarefas no turno inverso, seja trabalhando, estudando ou afins. Dessa forma, acaba sendo realmente muito difícil ir a uma monitoria. </p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center teal-text"><i class="material-icons large">home</i></h2>
                        <h5 class="center">Atendimento em sua casa</h5>

                        <p class="light center" style="font-size: 16px;">O percurso entre o IFRS e a casa do aluno, muitas vezes é bem cansativo, ainda mais se for no turno inverso. Para auxiliar os estudos destes alunos, será possível ter esse atendimento direto em casa.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center teal-text"><i class="material-icons large">settings</i></h2>
                        <h5 class="center">Materiais Didáticos</h5>

                        <p class="light center" style="font-size: 16px;">Quando o aluno estiver com um problema, ele poderá enviar um ticket de atendimento ao monitor da disciplina correspondente. Esse ticket chegará em forma de pendência, e ele deverá elaborar um material didático esclarecendo essa dúvida.</p></div>
                </div>
            </div>
        </div>

        <div class="section">
            <h3 class="center teal-text" id="equipe">QUEM SOMOS</h3><br><br><br>
            <div class="row">
                <div class="col s7 m12 l2 offset-m3 offset-s2 offset-l2" style="width: 300px;">
                    <div class="image-container">
                        <img src="view/imagens/GabrielViegas.jpg" class="circle responsive-img">
                    </div>
                </div>
                <div class="col s7 l4 m6 offset-m3 offset-s2 ">
                    <h5>Gabriel Viégas da Silva</h5>
                    <p>Desenvolvedor da plataforma e estudante do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul Campus Canoas, atualmente no 4° ano de informática.</p>
                </div>
            </div><br><br>
            <div class="row">
                <div class="col s7 m12 l2 offset-m3 offset-s2 offset-l2" style="width: 300px;">
                    <div class="image-container">
                        <img src="view/imagens/IgorLorenzato.jpg" class="circle responsive-img">
                    </div>
                </div>
                <div class="col s7 l4 m6 offset-m3 offset-s2">
                    <h5>Ígor Lorenzato Almeida</h5>
                    <p>Orientador do projeto e professor do Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul Campus Canoas.</p>
                </div>
            </div>
        </div>


        <br><br>


        <footer class="page-footer" style="background: #1E1F33;">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Endereço</h5>
                        <p class="grey-text text-lighten-4">R. Dra. Maria Zélia Carneiro de Figueiredo, 870 - A - Igara, Canoas - RS, 92412-240</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Entre em contato:</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">viegasgabriel156@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © Instituto Federal de Ciência e Tecnologia Campos Canoas
                </div>
            </div>
        </footer> 


        <?php
        if (!empty($_SESSION['sweet'])):
            if ($_SESSION['sweet'] == "Muito obrigado pela sugestão, ela será avaliada atentamente :) ") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você enviou uma sugestão!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Que droga, aposto que era algo muito útil!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>


    </body>
</html>
